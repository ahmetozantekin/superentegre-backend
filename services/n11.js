var http = require("https");
var express = require("express")
var fs = require('fs');
var format = require('xml-formatter');
var parser = require('xml2json');


var app = express();
var appKey = '61a40acd-358d-41d4-aa71-cb7e202018d6';
var appSecret = '8hSF9sneqF4G6H4b';
var productArray;

var options = {
    "method": "POST",
    "hostname": "api.n11.com",
    "port": null,
    "path": "/ws/ProductService.wsdl?appKey=" + appKey + "&appSecret=" + appSecret,
    "headers": {
        "content-type": "text/xml",
        "cache-control": "no-cache",
    }
};

var req = http.request(options, function (res) {
    var n11ProductData = [];

    res.on("data", function (xml) {
        n11ProductData.push(xml);
    });

    res.on("end", function () {
        var buf = Buffer.concat(n11ProductData);
        var body = buf.toString();

        var formattedXml = format(body);
        var json = parser.toJson(formattedXml);
        var obj = JSON.parse(json);
        productArray = obj['env:Envelope']['env:Body']['ns3:GetProductListResponse']['products']['product'];

        // fs.writeFile('products.json', json, function () {
        //     fs.readFile('./products.json', 'utf8', function (err, data) {
        //         if (err) throw err;
        //         var obj = JSON.parse(data);
        //         productArray = obj['env:Envelope']['env:Body']['ns3:GetProductListResponse']['products']['product'];
        //     });
        // });

    });

});

var soapResponse =
    '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sch="http://www.n11.com/ws/schemas">' +
    '<soapenv:Header/>' +
    '<soapenv:Body>' +
    '<sch:GetProductListRequest>' +
    '<auth>' +
    '<appKey>' + appKey + '</appKey>' +
    '<appSecret>' + appSecret +'</appSecret>' +
    '</auth>' +
    '<pagingData>' +
    '<currentPage>0</currentPage>' +
    '<pageSize>100</pageSize>' +
    '</pagingData>' + 
    '</sch:GetProductListRequest>' +
    '</soapenv:Body>' +
    '</soapenv:Envelope>';


req.write(soapResponse);
req.end();


app.get('/', function (req, res) {
    res.json(productArray);
})


app.listen(3003, function () { console.log(':3003 🚀 ') });