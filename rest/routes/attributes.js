
// Router
var express = require('express');
var router = express.Router();

var token = require('../controllers/token');
var attributes = require('../controllers/attributes');

router.get('/createCollection', function (req, res) {
    // attributes.createCollection();
    res.status(200).json({
        'status': res.statusCode,
        'success': true,
        'message': 'Successfull'
    });
})


/**
 * 
 * @api {Get} /attributes/all Get All Attributes
 * @apiName Get All Attributes
 * @apiGroup Attributes
 * @apiHeader {String} auth Users client_id
 * @apiVersion 0.0.1
 * 
 */
router.get('/all', function (req, res) {

    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth

    token.control(auth_token)
        .then(function (result) {
            if (result) {
                attributes.getAll()
                    .then(function (result) {
                        res.json(result);
                    })

            } else {
                res.status(401).json({
                    'status': res.statusCode,
                    'status_control': '04',
                    'success': false,
                    'message': 'Authorization Error'
                });
            }
        })
        .catch(function (err) {
            res.status(401).json({
                'status': res.statusCode,
                'status_control': '03',
                'success': false,
                'message': 'Authorization Error'
            });
        })

})


/**
 * 
 * @api {Post} /attributes/add Add Attributes
 * @apiName Add Attributes
 * @apiGroup Attributes
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 * 
 * @apiParam {Object} data Attributes Data Object
 */
router.post('/add', function (req, res) {

    var addObj = req.body;

    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth

    token.control(auth_token)
        .then(function (result) {
            if (result) {
                attributes.addListItem(addObj)
                    .then(function (result) {
                        if (result) {
                            res.status(200).json({
                                'status': res.statusCode,
                                'status_control': '01',
                                'success': true,
                                'message': 'Attribute save successfull'
                            });
                        }
                    })
                    .catch(function (err) {
                        res.status(404).json({
                            'status': res.statusCode,
                            'status_control': '03',
                            'success': false,
                            'message': 'Error'
                        });
                    })

            } else {
                res.status(401).json({
                    'status': res.statusCode,
                    'status_control': '04',
                    'success': false,
                    'message': 'Authorization Error'
                });
            }
        })
        .catch(function (err) {
            res.status(401).json({
                'status': res.statusCode,
                'status_control': '04',
                'success': false,
                'message': 'Authorization Error'
            });
        })

})


/**
 * 
 * @api {Get} /attributes/findbyid Find Attr By Id
 * @apiName Find Attr By Id
 * @apiGroup Attributes
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 * 
 * @apiParam {Object} data Attributes Data Object
 */
router.get('/findbyid', function (req, res) {

    var id = req.query.id;
    attributes.findById(id)
        .then(function (result) {
            res.json(result);
        })
        .catch(function (err) {
            res.status(404).json({
                'status': res.statusCode,
                'success': false,
                'message': 'Error'
            });
        })

})


/**
 *  @Private Route 
 */
router.get('/clearall', function (req, res) {
    attributes.clearAll()
        .then(function (result) {
            if (result) {
                res.status(200).json({
                    'status': res.statusCode,
                    'success': false,
                    'message': 'Remove attributes successful'
                });
            }
        })
        .catch(function (err) {
            res.status(404).json({
                'status': res.statusCode,
                'success': false,
                'message': "Attribute Not Found"
            });
        })

})


module.exports = router;