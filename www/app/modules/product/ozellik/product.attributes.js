angular.module('Ozellik')

    .controller('OzellikController', function ($scope, $http, $location, authService, attributesService) {
        $scope.paramCount = 0;
        $scope.load2 = true;

        const SERVICE_URL = "http://localhost:3001";
        
        if (authService.isLoggedIn) {

        } else {
            $location.path('/user/giris');
        }

        $scope.modalclose = function() {
            document.getElementsByClassName('modal')[0].style.display = "none"
            document.getElementsByClassName('modal-overlay')[0].style.display = "none"
        }
        
        $scope.getAll = function() {
            document.getElementsByClassName('modal')[0].style.display = "block"
            document.getElementsByClassName('modal-overlay')[0].style.display = "block"
            
            attributesService.getall(authService.authKey)
            .then(function (result) {
                console.log(result)
                if (result.status == 200){
                    $scope.load = false;
                    $scope.ul2 = true;
                    $scope.queryResult = result.data;
                    $scope.load2 = false;
                    console.log($scope.queryResult);
                } else {
                    console.log(result.message);
                }

                });
        }
         
        $scope.addParam = function () {

            // .directive olarak bi daha yazılır.
            template =
                '<div class="param" style="padding: 10px 0;border-bottom: 1px solid #333;">' +
                '<div class="form-group">' +
                '<label for="aParamName' + $scope.paramCount + '"> Parametre Adı: </label>' +
                '<input type="text" class="form-control" id="aParamName' + $scope.paramCount + '" ng-model="aParamName+' + $scope.paramCount + '">' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="aParamValue' + $scope.paramCount + '"> Parametre Değeri: </label>' +
                '<input type="text" class="form-control" id="aParamValue' + $scope.paramCount + '" ng-model="aParamValue' + $scope.paramCount + '">' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="aParamDescription' + $scope.paramCount + '"> Parametre Açıklama: </label>' +
                '<input type="text" class="form-control" id="aParamDescription' + $scope.paramCount + '" ng-model="aParamDescription' + $scope.paramCount + '">' +
                '</div>' +
                '</div> </hr>';


            tempElement = document.createElement('div');
            tempElement.innerHTML = template;
            document.getElementById('paramcontentid').appendChild(tempElement.firstChild);
            $scope.paramCount++;
        }

        $scope.newAttr = function () {
            var values = [];
            for (var i = 0; i < $scope.paramCount; i++) {
                values.push(
                    {
                        'id': i,
                        'name': document.getElementById('aParamName' + i).value,
                        'value': document.getElementById('aParamValue' + i).value,
                        'description': document.getElementById('aParamDescription' + i).value,
                        'is_active': true,
                    }
                )
            }

            var attrObj = {
                'name': $scope.aName,
                'description': $scope.aDescription,
                'display_order': 1,
                'is_active': true,
                'values': values
            }

            attributesService.addAttr(attrObj, authService.authKey)
                .then(function (result) {
                    if (result.data.status_control == "01") {
                        console.log('Attributes added successfuly');
                        $location.path('/product/urunler');
                    } else {
                        console.log(result.data.message)
                    }

                });
        }

    }); 