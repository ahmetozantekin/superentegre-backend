angular.module('Urunler')
 

  .controller('UrunlerController', function ($scope, $http, $location, authService, productService) {
    $scope.options = false;
    $scope.allLists = []; 
    $scope.allAttr = [];
    $scope.selectedAttr = [];
    $scope.paramCount = 1;

    const SERVICE_URL = "http://localhost:3001";

    if (authService.isLoggedIn) {
    } else {
      $location.path('/user/giris');
    }


    $scope.$on('$viewContentLoaded', function () {
      $scope.getAllLists();
      $scope.getAttrs();
    });

    // $scope.getNewAttrInputs = function () {

    //   var template =
    //     '<div class="row">' +
    //     '<div class="col-sm-8">' +
    //     '<select class="form-control" ng-model="attrselect' + $scope.paramCount + '" ng-change="getAttrValues()">' +
    //     '<option value="{{options._id}}" ng-repeat="options in allAttr">' +
    //     '{{options.name}}' +
    //     '</option>' +
    //     '</select>' +
    //     '</div>' +
    //     '<div class="col-sm-4">' +
    //     '<select class="form-control" ng-show="options" ng-model="attrselectvalue' + $scope.paramCount + '">' +
    //     '<option value="{{i.id}}" ng-repeat="i in selectedAttr">' +
    //     '{{i.value}}' +
    //     '</option>' +
    //     '</select>' +
    //     '</div>' +
    //     '</div>';
    //     console.log(template)

    //     tempElement = document.createElement('div');
    //     tempElement.innerHTML = template;

    //     document.getElementById('attrAarea').appendChild(tempElement.firstChild);
    //     $scope.paramCount++;
    // }
    $scope.test =  function(){
      var attrobj = JSON.parse($scope.attrselectvalue)
      console.log(attrobj.description)
    }
    $scope.getAttrValues = function () {
      $scope.options = true;
      $scope.selectedAttr = [];
      var attrObject = JSON.parse($scope.attrselect)
      productService.getAttrValues(attrObject._id, authService.authKey)
        .then(function (result) {
          var subs = [];
          if (result.status = 200) {
            $scope.subValues = result.data;
            for (var j = 0; j < $scope.subValues[0].values.length; j++) {
              $scope.selectedAttr.push($scope.subValues[0].values[j])
            }
          } else {
            console.log('Hata.')
          }
        });
    }

    $scope.getAttrs = function () {

      productService.allAttr(authService.authKey)
        .then(function (result) {
          if (result.status = 200) {
            $scope.allAttr = result.data;
          } else {
            alert('Error')
          }
        });
    }

    $scope.getAllLists = function () {
      var req = {
        'method': "GET",
        'url': SERVICE_URL + "/lists/categories",
        'headers': {
          'auth': authService.client_id
        }
      }
      $http(req)
        .then(function (result) {
          if (result.data.status_control = 200) {
            $scope.allLists = result.data;
            var cats = [];
            for (list in result.data) {
              cats.push(result.data[list].categories)
            }
            $scope.allLists = cats;
          } else {
            console.log("err");
          }
        });
    }

    $scope.newList = function () {
      var name             = $scope.pName;
      var shortdescription = $scope.pShortDescription;
      var fulldescription  = $scope.pFullDescription;
      var quantity         = $scope.pQuantity;
      var barcode          = $scope.pBarcode;
      var stockcode        = $scope.pStockCode;
      var sku              = $scope.pSku;
      var brandname        = $scope.pBrandName;
      var brandcode        = $scope.pBrandCode;
      var weight           = $scope.pWeight;
      var length           = $scope.pLength;
      var width            = $scope.pWidth;
      var height           = $scope.pHeight;
      var price            = $scope.pPrice;
      var saleprice        = $scope.pSalePrice;
      var currency         = $scope.pCurrency;
      var saleenddate      = $scope.pSaleEndDate;
      var salestartdate    = $scope.pSaleStartDate;
      var category         = $scope.pLists;
      var attrObject       = JSON.parse($scope.attrselect);
      var attrSelect       = JSON.parse($scope.attrselectvalue);
      var date = new Date();


      var konsolObj = {
        'name': name,
        'group_code' : '',
        'shortdescription': shortdescription,
        'fulldescription': fulldescription,
        'variants': {
          'id': '',
          'quantity': quantity,
          'barcode ': barcode,
          'stockcode': stockcode,
          'sku': sku,
          'attributes': {
              'attribute_id' : attrObject._id,
              'name' : attrObject.name,
              'value' : attrSelect.value,
              'attibute_value_id' : attrSelect.id,
              'is_active' : true,
              'created_date' : date,
              'updated_date' : date
          },
          'price': {
            'id': '',
            'price': price,
            'sale_price': saleprice,
            'currency ': currency,
            'sale_end_date': saleenddate,
            'sale_start_date': salestartdate,
          }
        },
        'is_active': true,
        'brand': {
          'name': brandname,
          'code': brandcode
        },
        'created_date': date,
        'updated_date': date,
        'shipping': {
          'weight': weight,
          'length': length,
          'width': width,
          'height': height,
        },
        'images': {
          'image': {
            'path': '',
            'is_active': true,
            'display_order': 1,
            'created_date': date,
            "type": ''
          }
        },
        'lists': category
      }

      var req = {
        'method': "POST",
        'url': SERVICE_URL + "/catalogue/add",
        'data': konsolObj,
        'headers': {
          'auth': authService.authKey
        }
      }

////////////////////////////// BURADAN DEVAM EDİLECEK


      $http(req)
        .then(function (result) {
          if (result.status = 200) {
            // RESPONSE DONDURULECEK.
            // DONDUR.
            alert('KAYIT BAŞARILI')
          } else {
            alert('Error')
          }
        });

    }
  })


  /*
  *
  * input mask directive
  *
  */

  // .directive('currencyMask', function() {
  //   return {
  //     restrict: 'A',
  //     require: 'ngModel',
  //     link: function(scope, element, attrs, ngModelController) {
        
  //       var formatNumber = function(value) {
       
  //         value = value.toString();
  //         value = value.replace(/[^0-9\.]/g, "");
  //         var parts = value.split('.');
  //         parts[0] = parts[0].replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, "$&,");
  //         if (parts[1] && parts[1].length > 2) {
  //           parts[1] = parts[1].substring(0, 2);
  //         }
         
  //         return parts.join(".");
  //       };
  //       var applyFormatting = function() {
  //         var value = element.val();
  //         var original = value;
  //         if (!value || value.length == 0) {
  //           return
  //         }
  //         value = formatNumber(value);
  //         if (value != original) {
  //           element.val(value);
  //           element.triggerHandler('input')
  //         }
  //       };
  //       element.bind('keyup', function(e) {
  //         var keycode = e.keyCode;
  //         var isTextInputKey =
  //           (keycode > 47 && keycode < 58) || // number keys
  //           keycode == 32 || keycode == 8 || // spacebar or backspace
  //           (keycode > 64 && keycode < 91) || // letter keys
  //           (keycode > 95 && keycode < 112) || // numpad keys
  //           (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
  //           (keycode > 218 && keycode < 223); // [\]' (in order)
  //         if (isTextInputKey) {
  //           applyFormatting();
  //         }
  //       });
  //       element.bind('blur', function(evt) {
  //         if (angular.isDefined(ngModelController.$modelValue)) {
  //           var val = ngModelController.$modelValue.split('.');
  //           if (val && val.length == 1) {
  //             if (val != "") {
  //               ngModelController.$setViewValue(val + '.00');
  //               ngModelController.$render();
  //             }
  //           } else if (val && val.length == 2) {
  //             if (val[1] && val[1].length == 1) {
  //               ngModelController.$setViewValue(val[0] + '.' + val[1] + '0');
  //               ngModelController.$render();
  //             } else if (val[1].length == 0) {
  //               ngModelController.$setViewValue(val[0] + '.00');
  //               ngModelController.$render();
  //             }
  //             applyFormatting();
  //           }
  //         }
  //       })
  //       ngModelController.$parsers.push(function(value) {
  //         if (!value || value.length == 0) {
  //           return value;
  //         }
  //         value = value.toString();
  //         value = value.replace(/[^0-9\.]/g, "");
  //         return value;
  //       });
  //       ngModelController.$formatters.push(function(value) {
  //         if (!value || value.length == 0) {
  //           return value;
  //         }
  //         value = formatNumber(value);
  //         return value;
  //       });
  //     }
  //   };
  // });
angular.module('Kategoriler', ['multipleSelect'])


    .controller('KategorilerYeniController', function ($scope, $http, $location, authService) {

        const SERVICE_URL = "http://localhost:3001";

        if (authService.isLoggedIn) {
        } else {
          $location.path('/user/giris');
        }

        $scope.ul2 = false;
        $scope.queryResult = [];
        $scope.paramCount = 1;

        $scope.$on('$viewContentLoaded', function () {
        });

      
        $scope.editLists = function (u, p) {
            $scope.load = true;
            var id = u;
            var name = p;

            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/lists/edit?id=" + id + "&name=" + name,
                'headers': {
                    'auth': authService.authKey
                }
            }

            $http(req)
                .then(function (result) {

                    if (result.status = 200) {
                        console.log('Düzenlendi')
                        $scope.getAll();
                    } else {
                        alert('Error')
                    }
                });
        }

        $scope.removeLists = function (u) {
            var id = u;

            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/lists/removebyid?id=" + id,
                'headers': {
                    'auth': authService.authKey
                }
            }

            $http(req)
                .then(function (result) {

                    if (result.status = 200) {
                        console.log('Silindi.')
                        $scope.getAll();
                    } else {
                        alert('Error')
                    }
                });


        }

        $scope.addParam = function () {

            // .directive olarak bi daha yazılır.
            template =
                '<div class="paramelem">' +
                '<div class="form-group">' +
                '<label for="newName' + $scope.paramCount + '"> Kategori Adı: </label>' +
                '<input type="text" class="form-control" id="newName' + $scope.paramCount + '" ng-model="newName+' + $scope.paramCount + '">' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="newParent' + $scope.paramCount + '"> Kategori Parent: </label>' +
                '<input type="text" class="form-control" id="newParent' + $scope.paramCount + '" ng-model="newParent' + $scope.paramCount + '">' +
                '</div>' +
                '</div></hr>';

            tempElement = document.createElement('div');
            tempElement.innerHTML = template;

            document.getElementById('paramlist').appendChild(tempElement.firstChild);
            $scope.paramCount++;
        }

        $scope.newList = function () {
            var listValues = [];
            for (var i = 0; i < $scope.paramCount; i++) {
                listValues.push(
                    {
                        //    kategori-adi-1
                        // önemli-kategori-2
                        //  örnek-kategori-3
                        'id': document.getElementById('newName' + i).value.toLowerCase().trim().split(' ').join('-') + '-' + i,
                        'name': document.getElementById('newName' + i).value,
                        'parent_list': document.getElementById('newParent' + i).value,
                    }
                )
            }
            var listsObject = {
                'client_id': authService.client_id,
                'categories': listValues
            }
            var req = {
                'method': "POST",
                'url': SERVICE_URL + "/lists/add",
                'data': listsObject,
                'headers': {
                    'auth': authService.authKey
                }
            }
            $http(req)
                .then(function (result) {
                    switch (result.data.status_control) {

                        case '01':
                            console.log(result.data.message);
                            $scope.getAll();
                            break;
                        case '03':
                            console.log(result.data.message);
                            break;
                        case '04':
                            console.log(result.data.message);
                            break;
                        case '05':
                            console.log(result.data.message);
                            break;

                    }
                });

        }

    });

angular.module('Kategoriler', ['multipleSelect'])


    .controller('KategorilerController', function ($scope, $http, $location, authService) {

        const SERVICE_URL = "http://localhost:3001";


        if (authService.isLoggedIn) {
        } else {
            $location.path('/user/giris');
        }

        $scope.ul2 = false;
        $scope.queryResult = [];
        $scope.paramCount = 1;
        $scope.categoryFromClient;

        $scope.$on('$viewContentLoaded', function () {
            $scope.getAll();
        });

        $scope.getAll = function () {
            $scope.load = true;

            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/lists/categories",
                'headers': {
                    'auth': authService.client_id
                }
            }

            $http(req)
                .then(function (result) {

                    switch (result.status) {
                        case 200:
                            $scope.load = false;
                            $scope.ul2 = true;
                            $scope.queryResult = result.data;

                            var jsonCat = [];
                            $scope.queryResult.forEach(function (key) {
                                jsonCat.push(key.categories);
                            })
                            $scope.categoryFromClient = jsonCat;
                            console.log( $scope.categoryFromClient);
                            break;
                        case 404:
                            console.log(result.data.message);
                            break;
                    }

                });
        }

        $scope.editLists = function (u, p) {
            $scope.load = true;
            var id = u;
            var name = p;

            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/lists/edit?id=" + id + "&name=" + name,
                'headers': {
                    'auth': authService.authKey
                }
            }

            $http(req)
                .then(function (result) {

                    if (result.status = 200) {
                        console.log('Düzenlendi')
                        $scope.getAll();
                    } else {
                        alert('Error')
                    }
                });
        }

        $scope.removeLists = function (u) {
            var id = u;
            console.log(id);

            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/lists/removebyid?id=" + id,
                'headers': {
                    'auth': authService.authKey
                }
            }

            $http(req)
                .then(function (result) {

                    if (result.status = 200) {
                        console.log('Silindi.')
                        $scope.getAll();
                    } else {
                        alert('Error')
                    }
                });


        }

        // $scope.addParam = function () {

        //     // .directive olarak bi daha yazılır.
        //     template =
        //         '<div class="paramelem">' +
        //         '<div class="form-group">' +
        //         '<label for="newName' + $scope.paramCount + '"> Kategori Adı: </label>' +
        //         '<input type="text" class="form-control" id="newName' + $scope.paramCount + '" ng-model="newName+' + $scope.paramCount + '">' +
        //         '</div>' +
        //         '<div class="form-group">' +
        //         '<label for="newParent' + $scope.paramCount + '"> Kategori Parent: </label>' +
        //         '<input type="text" class="form-control" id="newParent' + $scope.paramCount + '" ng-model="newParent' + $scope.paramCount + '">' +
        //         '</div>' +
        //         '</div></hr>';

        //     tempElement = document.createElement('div');
        //     tempElement.innerHTML = template;

        //     document.getElementById('paramlist').appendChild(tempElement.firstChild);
        //     $scope.paramCount++;
        // }

        $scope.newList = function () {
            $scope.load = true;
            if ($scope.newParent) {
                var catId = $scope.newParent.split(',')[0];
                var catName = $scope.newParent.split(',')[1];
            } else{
                catId = '';
                catName = '';
            }

            if (!$scope.newName) {
                alert('İsim Alanı Boş Olamaz.');
            } else {
                var listsObject = {
                    'client_id': authService.client_id,
                    'categories': {
                        'is_active': true,
                        'id': authService.client_id + $scope.newName.toLowerCase().trim().split(' ').join('-') + Math.floor(Math.random() * 1000),
                        'name': $scope.newName,
                        'parent_list': catName,
                        'parent_list_id': catId
                    }
                }
                var req = {
                    'method': "POST",
                    'url': SERVICE_URL + "/lists/add",
                    'data': listsObject,
                    'headers': {
                        'auth': authService.authKey
                    }
                }
                $http(req)
                    .then(function (result) {
                        switch (result.data.status_control) {

                            case '01':
                                console.log(result.data.message);
                                $scope.load = false;
                                $location.path('/lists/tumunugor');
                                break;
                            case '03':
                                console.log(result.data.message);
                                break;
                            case '04':
                                console.log(result.data.message);
                                break;
                            case '05':
                                console.log(result.data.message);
                                break;

                        }
                    });
            }

        }

    });

angular.module('Ozellik')

    .controller('OzellikController', function ($scope, $http, $location, authService, attributesService) {
        $scope.paramCount = 0;
        $scope.load2 = true;

        const SERVICE_URL = "http://localhost:3001";
        
        if (authService.isLoggedIn) {

        } else {
            $location.path('/user/giris');
        }

        $scope.modalclose = function() {
            document.getElementsByClassName('modal')[0].style.display = "none"
            document.getElementsByClassName('modal-overlay')[0].style.display = "none"
        }
        
        $scope.getAll = function() {
            document.getElementsByClassName('modal')[0].style.display = "block"
            document.getElementsByClassName('modal-overlay')[0].style.display = "block"
            
            attributesService.getall(authService.authKey)
            .then(function (result) {
                console.log(result)
                if (result.status == 200){
                    $scope.load = false;
                    $scope.ul2 = true;
                    $scope.queryResult = result.data;
                    $scope.load2 = false;
                    console.log($scope.queryResult);
                } else {
                    console.log(result.message);
                }

                });
        }
         
        $scope.addParam = function () {

            // .directive olarak bi daha yazılır.
            template =
                '<div class="param" style="padding: 10px 0;border-bottom: 1px solid #333;">' +
                '<div class="form-group">' +
                '<label for="aParamName' + $scope.paramCount + '"> Parametre Adı: </label>' +
                '<input type="text" class="form-control" id="aParamName' + $scope.paramCount + '" ng-model="aParamName+' + $scope.paramCount + '">' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="aParamValue' + $scope.paramCount + '"> Parametre Değeri: </label>' +
                '<input type="text" class="form-control" id="aParamValue' + $scope.paramCount + '" ng-model="aParamValue' + $scope.paramCount + '">' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="aParamDescription' + $scope.paramCount + '"> Parametre Açıklama: </label>' +
                '<input type="text" class="form-control" id="aParamDescription' + $scope.paramCount + '" ng-model="aParamDescription' + $scope.paramCount + '">' +
                '</div>' +
                '</div> </hr>';


            tempElement = document.createElement('div');
            tempElement.innerHTML = template;
            document.getElementById('paramcontentid').appendChild(tempElement.firstChild);
            $scope.paramCount++;
        }

        $scope.newAttr = function () {
            var values = [];
            for (var i = 0; i < $scope.paramCount; i++) {
                values.push(
                    {
                        'id': i,
                        'name': document.getElementById('aParamName' + i).value,
                        'value': document.getElementById('aParamValue' + i).value,
                        'description': document.getElementById('aParamDescription' + i).value,
                        'is_active': true,
                    }
                )
            }

            var attrObj = {
                'name': $scope.aName,
                'description': $scope.aDescription,
                'display_order': 1,
                'is_active': true,
                'values': values
            }

            attributesService.addAttr(attrObj, authService.authKey)
                .then(function (result) {
                    if (result.data.status_control == "01") {
                        console.log('Attributes added successfuly');
                        $location.path('/product/urunler');
                    } else {
                        console.log(result.data.message)
                    }

                });
        }

    }); 
angular.module('Giris')

    .controller('GirisController', function ($rootScope, $scope, $http, $location, authService) {
        $scope.n_username = '';
        $scope.n_password = '';
        $scope.errorbox = false;

        if( authService.isLoggedIn ){
            $location.path('/anasayfa/panel');
            
        } else {
            $location.path('/user/giris'); 
        }

        $scope.closeerror = function(){
            $scope.errorbox = false;
            $rootScope.correctbox = false;
        }
        $scope.loginUser = function () {
            var newUsername = $scope.n_username;
            var newPassword = $scope.n_password;

            authService.login(newUsername, newPassword)
                .then(function (result) { 

                    switch(result.data.status_control){

                        case '01' : 
                            // console.log(result.data.message);
                            authService.setLogin( result.data.token , result.data.client_id);
                            $location.path('/user/profil');
                            break;
                        case '03' : 
                            // console.log(result.data.message);
                            $scope.errorbox = true;
                            $rootScope.correctbox = false;
                            break;

                    }

                });
        }

        $scope.logoutUser = function(){
            authService.logout();
        }
    });
angular.module('Kayit', [])

    .controller('KayitController', function ($rootScope, $scope, $http, $location, authService) {
        $scope.errorbox = false;
        $scope.validerrorbox = false;
        $scope.validerror = '';

        if (authService.isLoggedIn) {
            $location.path('/anasayfa/panel');

        } else {
            $location.path('/user/kayit');
        }
        $scope.closeerror = function () {
            $scope.errorbox = false;
            $scope.validerrorbox = false;
        }
        $scope.signUpUser = function () {
            $scope.validerrorbox = false;
            var newRealName = $scope.n_realname;
            var newUsername = $scope.n_username.toLowerCase().replace(/\s+/g, '');
            var newPassword = $scope.n_password;
            var verifyPass = $scope.n_password2;
            var newMail = $scope.n_mail;

            const MAX_PASSWORD_LENGTH = 16;
            const MIN_PASSWORD_LENGTH = 3;

            // Form Validation Kontrol
            if (!newUsername || !newPassword || !newMail) {
                $scope.validerrorbox = true;
                $scope.validerror = 'Formda eksik bırakmayınız.';

            }
            else {
                if (newPassword.length >= MIN_PASSWORD_LENGTH && newPassword.length <= MAX_PASSWORD_LENGTH) {

                    if (newPassword == verifyPass) {
                        authService.signup(newUsername, newMail, newPassword, newRealName)
                            .then(function (result) {

                                switch (result.data.status_control) {
                                    case '01':
                                        console.log('Kayıt Başarılı Girişe Yönleniyoruz');
                                        $rootScope.correctbox = true;
                                        $location.path('/user/giris');
                                        break;

                                    case '05':
                                        $scope.errorbox = true;
                                        console.log(result.data.message);
                                        break;

                                    case '07':
                                        $scope.errorbox = true;
                                        console.log(result.data.message);
                                        break;

                                    case '03':
                                        $scope.errorbox = true;
                                        console.log(result.data.message);
                                        break;
                                }

                            });
                    } else {
                        // Password 3 karakterden az 16 karakterden fazla olamaz.
                        $scope.validerrorbox = true;
                        $scope.validerror  = 'Şifreler Uyuşmuyor.';
                        $scope.n_password  = '';
                        $scope.n_password2 = '';
                  
                    }

                } else {
                    // Password 3 karakterden az 16 karakterden fazla olamaz.
                    $scope.validerrorbox = true;
                    $scope.validerror = 'Şifre 3 Karakterden Az, 16 Karakterden Fazla Olmamalıdır.';
                    $scope.n_password  = '';
                    $scope.n_password2 = '';
                }

            }

        }

    })

angular.module('TumunuGor', [])

    .controller('TumunuGorController', function ($scope, $http, $location, authService, md5, $filter) {
        $scope.load = true;
        $scope.ul = false;
        $scope.errorbox = false;
        $scope.validerrorbox = false;
        $scope.correctbox = false;
        $scope.validerror = '';
        $scope.mesaj = "Tümünü Gör";
        $scope.queryResult = [];
        $scope.UserFromIdResult = [];
        $scope.pass;
        $scope.u_username;

        const SERVICE_URL = "http://localhost:3001";
        const MAX_PASSWORD_LENGTH = 16;
        const MIN_PASSWORD_LENGTH = 3;

        if (authService.isLoggedIn) {
        } else {
            $location.path('/user/giris');
        }

        $scope.closeerror = function () {
            $scope.correctbox = false;
            $scope.errorbox = false;
            $scope.validerrorbox = false;
        }

        $scope.UserFromIdResult = function () {
            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/user/findbyid?id=" + authService.client_id,
                'headers': {
                    'auth': authService.authKey
                }
            }
            $http(req)
                .then(function (result) {
                    if (result.status = 200) {
                        $scope.UserFromIdResult = result.data[0];
                        console.log($scope.UserFromIdResult);
                        
                        $scope.MembershipStartDate = $filter('date')($scope.UserFromIdResult.membership_start_day, 'dd.MM.yyyy') + ' tarihinden beri üye';
                        $scope.pass = $scope.UserFromIdResult.password;
                        $scope.u_username = $scope.UserFromIdResult.username;
                        var hash = md5.createHash($scope.UserFromIdResult.email);
                        $scope.useGravatar = 'https://tr.gravatar.com/avatar/' + hash + '?s=130.json';
                    } else {
                        alert('Error')
                    }
                });
        }


        $scope.changeUsername = function () {
            if (!$scope.u_username || !$scope.u_pass) {
                $scope.validerror = 'Formda Eskik Bırakmayınız.';
                $scope.closeerror();
                $scope.validerrorbox = true;
            } else {
                if($scope.u_pass != $scope.pass){
                    $scope.validerror = 'Şifre Hatalı.';
                    $scope.closeerror();
                    $scope.validerrorbox = true;
                } else {
                    if($scope.u_pass.length >= MIN_PASSWORD_LENGTH && $scope.u_pass.length <= MAX_PASSWORD_LENGTH){
                        $scope.closeerror();
                        var req = {
                            'method': "GET",
                            'url': SERVICE_URL + "/user/edit?username=" + $scope.u_username + "&password=" + $scope.u_pass,
                            'headers': {
                                'auth': authService.authKey
                            }
                        }
            
                        $http(req)
                            .then(function (result) {
                                if (result.status = 200) {
                                    $scope.correctbox = true;
                                    $scope.u_username = '';
                                    $scope.u_pass   = '';
                                } else {
                                    $scope.closeerror();
                                    $scope.validerror = 'Bir Hata ile Karşılaştık.';
                                    $scope.validerrorbox = true;
                                }
                            });
                    } else {
                        $scope.closeerror();
                        $scope.validerror = 'Şifre 3 Karakterden Az, 16 Karakterden Fazla Olmamalıdır.';
                        $scope.validerrorbox = true; 
                    }
                }
             
            }
        }

        $scope.changePassword = function () {
    
            if (!$scope.o_pass || !$scope.n_pass || !$scope.n_r_pass) {
                $scope.validerror = 'Formda Eskik Bırakmayınız.';
                $scope.closeerror();
                $scope.validerrorbox = true;
            } else {
                if ($scope.n_r_pass != $scope.n_pass) {
                    $scope.closeerror();
                    $scope.errorbox = true;
                } else {
                    if ($scope.o_pass != $scope.pass) {
                        $scope.closeerror();
                        $scope.validerror = 'Eski Şifreniz ile Girdiğiniz Şifre Uyuşmuyor.';
                        $scope.validerrorbox = true;
                    } else {
                        if ($scope.o_pass == $scope.n_pass) {
                            $scope.closeerror();
                            $scope.validerror = 'Eski Şifreniz ile Yeni Şifreniz Aynı.';
                            $scope.validerrorbox = true;
                        } else {
                            if($scope.n_pass.length >= MIN_PASSWORD_LENGTH && $scope.n_pass.length <= MAX_PASSWORD_LENGTH){
                         
                                $scope.closeerror();
                                var req = {
                                    'method': "GET",
                                    'url': SERVICE_URL + "/user/edit?username=" + $scope.UserFromIdResult.username + "&password=" + $scope.n_pass,
                                    'headers': {
                                        'auth': authService.authKey
                                    }
                                }
                    
                                $http(req)
                                    .then(function (result) {
                                        if (result.status = 200) {
                                            $scope.correctbox = true;
                                            $scope.o_pass   = '';
                                            $scope.n_pass   = '';
                                            $scope.n_r_pass = '';
                                        } else {
                                            $scope.closeerror();
                                            $scope.validerror = 'Bir Hata ile Karşılaştık.';
                                            $scope.validerrorbox = true;
                                        }
                                    });
                            } else {
                                $scope.closeerror();
                                $scope.validerror = 'Şifre 3 Karakterden Az, 16 Karakterden Fazla Olmamalıdır.';
                                $scope.validerrorbox = true; 
                                
                            }
                          
                        }
                    }
                }
                
            }

        }

        $scope.getAll = function () {
            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/user/all",
                'headers': {
                    'auth': authService.authKey
                }
            }

            $http(req)
                .then(function (result) {

                    if (result.status = 200) {
                        $scope.load = false;
                        $scope.ul = true;
                        $scope.queryResult = result.data;
                    } else {
                        alert('Error')
                    }
                });

        }

        $scope.$on('$viewContentLoaded', function () {
            $scope.getAll();
            $scope.UserFromIdResult();
        });

        $scope.editUser = function (u, p) {
            var token = this.item.token.key;
            var newUsername = u;
            var newPass = p;

            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/user/edit?username=" + newUsername + "&password=" + newPass,
                'headers': {
                    'auth': token
                }
            }

            $http(req)
                .then(function (result) {
                    if (result.status = 200) {
                        console.log('Düzenlendi')
                        $scope.getAll();
                    } else {
                        alert('Error')
                    }
                });
        }

        $scope.removeUser = function () {
            var token = this.item._id;
            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/user/removebyid?id=" + token,
                'headers': {
                    'auth': authService.authKey
                }
            }
            $http(req)
                .then(function (result) {
                    if (result.status = 200) {
                        $scope.getAll();
                    } else {
                        alert('Error')
                    }
                });
        }

        $scope.logoutUser = function () {
            authService.logout();
            $location.path('/user/giris');
        }


    });