angular.module('TumunuGor', [])

    .controller('TumunuGorController', function ($scope, $http, $location, authService, md5, $filter) {
        $scope.load = true;
        $scope.ul = false;
        $scope.errorbox = false;
        $scope.validerrorbox = false;
        $scope.correctbox = false;
        $scope.validerror = '';
        $scope.mesaj = "Tümünü Gör";
        $scope.queryResult = [];
        $scope.UserFromIdResult = [];
        $scope.pass;
        $scope.u_username;

        const SERVICE_URL = "http://localhost:3001";
        const MAX_PASSWORD_LENGTH = 16;
        const MIN_PASSWORD_LENGTH = 3;

        if (authService.isLoggedIn) {
        } else {
            $location.path('/user/giris');
        }

        $scope.closeerror = function () {
            $scope.correctbox = false;
            $scope.errorbox = false;
            $scope.validerrorbox = false;
        }

        $scope.UserFromIdResult = function () {
            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/user/findbyid?id=" + authService.client_id,
                'headers': {
                    'auth': authService.authKey
                }
            }
            $http(req)
                .then(function (result) {
                    if (result.status = 200) {
                        $scope.UserFromIdResult = result.data[0];
                        console.log($scope.UserFromIdResult);
                        
                        $scope.MembershipStartDate = $filter('date')($scope.UserFromIdResult.membership_start_day, 'dd.MM.yyyy') + ' tarihinden beri üye';
                        $scope.pass = $scope.UserFromIdResult.password;
                        $scope.u_username = $scope.UserFromIdResult.username;
                        var hash = md5.createHash($scope.UserFromIdResult.email);
                        $scope.useGravatar = 'https://tr.gravatar.com/avatar/' + hash + '?s=130.json';
                    } else {
                        alert('Error')
                    }
                });
        }


        $scope.changeUsername = function () {
            if (!$scope.u_username || !$scope.u_pass) {
                $scope.validerror = 'Formda Eskik Bırakmayınız.';
                $scope.closeerror();
                $scope.validerrorbox = true;
            } else {
                if($scope.u_pass != $scope.pass){
                    $scope.validerror = 'Şifre Hatalı.';
                    $scope.closeerror();
                    $scope.validerrorbox = true;
                } else {
                    if($scope.u_pass.length >= MIN_PASSWORD_LENGTH && $scope.u_pass.length <= MAX_PASSWORD_LENGTH){
                        $scope.closeerror();
                        var req = {
                            'method': "GET",
                            'url': SERVICE_URL + "/user/edit?username=" + $scope.u_username + "&password=" + $scope.u_pass,
                            'headers': {
                                'auth': authService.authKey
                            }
                        }
            
                        $http(req)
                            .then(function (result) {
                                if (result.status = 200) {
                                    $scope.correctbox = true;
                                    $scope.u_username = '';
                                    $scope.u_pass   = '';
                                } else {
                                    $scope.closeerror();
                                    $scope.validerror = 'Bir Hata ile Karşılaştık.';
                                    $scope.validerrorbox = true;
                                }
                            });
                    } else {
                        $scope.closeerror();
                        $scope.validerror = 'Şifre 3 Karakterden Az, 16 Karakterden Fazla Olmamalıdır.';
                        $scope.validerrorbox = true; 
                    }
                }
             
            }
        }

        $scope.changePassword = function () {
    
            if (!$scope.o_pass || !$scope.n_pass || !$scope.n_r_pass) {
                $scope.validerror = 'Formda Eskik Bırakmayınız.';
                $scope.closeerror();
                $scope.validerrorbox = true;
            } else {
                if ($scope.n_r_pass != $scope.n_pass) {
                    $scope.closeerror();
                    $scope.errorbox = true;
                } else {
                    if ($scope.o_pass != $scope.pass) {
                        $scope.closeerror();
                        $scope.validerror = 'Eski Şifreniz ile Girdiğiniz Şifre Uyuşmuyor.';
                        $scope.validerrorbox = true;
                    } else {
                        if ($scope.o_pass == $scope.n_pass) {
                            $scope.closeerror();
                            $scope.validerror = 'Eski Şifreniz ile Yeni Şifreniz Aynı.';
                            $scope.validerrorbox = true;
                        } else {
                            if($scope.n_pass.length >= MIN_PASSWORD_LENGTH && $scope.n_pass.length <= MAX_PASSWORD_LENGTH){
                         
                                $scope.closeerror();
                                var req = {
                                    'method': "GET",
                                    'url': SERVICE_URL + "/user/edit?username=" + $scope.UserFromIdResult.username + "&password=" + $scope.n_pass,
                                    'headers': {
                                        'auth': authService.authKey
                                    }
                                }
                    
                                $http(req)
                                    .then(function (result) {
                                        if (result.status = 200) {
                                            $scope.correctbox = true;
                                            $scope.o_pass   = '';
                                            $scope.n_pass   = '';
                                            $scope.n_r_pass = '';
                                        } else {
                                            $scope.closeerror();
                                            $scope.validerror = 'Bir Hata ile Karşılaştık.';
                                            $scope.validerrorbox = true;
                                        }
                                    });
                            } else {
                                $scope.closeerror();
                                $scope.validerror = 'Şifre 3 Karakterden Az, 16 Karakterden Fazla Olmamalıdır.';
                                $scope.validerrorbox = true; 
                                
                            }
                          
                        }
                    }
                }
                
            }

        }

        $scope.getAll = function () {
            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/user/all",
                'headers': {
                    'auth': authService.authKey
                }
            }

            $http(req)
                .then(function (result) {

                    if (result.status = 200) {
                        $scope.load = false;
                        $scope.ul = true;
                        $scope.queryResult = result.data;
                    } else {
                        alert('Error')
                    }
                });

        }

        $scope.$on('$viewContentLoaded', function () {
            $scope.getAll();
            $scope.UserFromIdResult();
        });

        $scope.editUser = function (u, p) {
            var token = this.item.token.key;
            var newUsername = u;
            var newPass = p;

            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/user/edit?username=" + newUsername + "&password=" + newPass,
                'headers': {
                    'auth': token
                }
            }

            $http(req)
                .then(function (result) {
                    if (result.status = 200) {
                        console.log('Düzenlendi')
                        $scope.getAll();
                    } else {
                        alert('Error')
                    }
                });
        }

        $scope.removeUser = function () {
            var token = this.item._id;
            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/user/removebyid?id=" + token,
                'headers': {
                    'auth': authService.authKey
                }
            }
            $http(req)
                .then(function (result) {
                    if (result.status = 200) {
                        $scope.getAll();
                    } else {
                        alert('Error')
                    }
                });
        }

        $scope.logoutUser = function () {
            authService.logout();
            $location.path('/user/giris');
        }


    });