angular.module('Urunler')
 

  .controller('UrunlerController', function ($scope, $http, $location, authService, productService) {
    $scope.options = false;
    $scope.allLists = []; 
    $scope.allAttr = [];
    $scope.selectedAttr = [];
    $scope.paramCount = 1;

    const SERVICE_URL = "http://localhost:3001";

    if (authService.isLoggedIn) {
    } else {
      $location.path('/user/giris');
    }


    $scope.$on('$viewContentLoaded', function () {
      $scope.getAllLists();
      $scope.getAttrs();
    });

    // $scope.getNewAttrInputs = function () {

    //   var template =
    //     '<div class="row">' +
    //     '<div class="col-sm-8">' +
    //     '<select class="form-control" ng-model="attrselect' + $scope.paramCount + '" ng-change="getAttrValues()">' +
    //     '<option value="{{options._id}}" ng-repeat="options in allAttr">' +
    //     '{{options.name}}' +
    //     '</option>' +
    //     '</select>' +
    //     '</div>' +
    //     '<div class="col-sm-4">' +
    //     '<select class="form-control" ng-show="options" ng-model="attrselectvalue' + $scope.paramCount + '">' +
    //     '<option value="{{i.id}}" ng-repeat="i in selectedAttr">' +
    //     '{{i.value}}' +
    //     '</option>' +
    //     '</select>' +
    //     '</div>' +
    //     '</div>';
    //     console.log(template)

    //     tempElement = document.createElement('div');
    //     tempElement.innerHTML = template;

    //     document.getElementById('attrAarea').appendChild(tempElement.firstChild);
    //     $scope.paramCount++;
    // }
    $scope.test =  function(){
      var attrobj = JSON.parse($scope.attrselectvalue)
      console.log(attrobj.description)
    }
    $scope.getAttrValues = function () {
      $scope.options = true;
      $scope.selectedAttr = [];
      var attrObject = JSON.parse($scope.attrselect)
      productService.getAttrValues(attrObject._id, authService.authKey)
        .then(function (result) {
          var subs = [];
          if (result.status = 200) {
            $scope.subValues = result.data;
            for (var j = 0; j < $scope.subValues[0].values.length; j++) {
              $scope.selectedAttr.push($scope.subValues[0].values[j])
            }
          } else {
            console.log('Hata.')
          }
        });
    }

    $scope.getAttrs = function () {

      productService.allAttr(authService.authKey)
        .then(function (result) {
          if (result.status = 200) {
            $scope.allAttr = result.data;
          } else {
            alert('Error')
          }
        });
    }

    $scope.getAllLists = function () {
      var req = {
        'method': "GET",
        'url': SERVICE_URL + "/lists/categories",
        'headers': {
          'auth': authService.client_id
        }
      }
      $http(req)
        .then(function (result) {
          if (result.data.status_control = 200) {
            $scope.allLists = result.data;
            var cats = [];
            for (list in result.data) {
              cats.push(result.data[list].categories)
            }
            $scope.allLists = cats;
          } else {
            console.log("err");
          }
        });
    }

    $scope.newList = function () {
      var name             = $scope.pName;
      var shortdescription = $scope.pShortDescription;
      var fulldescription  = $scope.pFullDescription;
      var quantity         = $scope.pQuantity;
      var barcode          = $scope.pBarcode;
      var stockcode        = $scope.pStockCode;
      var sku              = $scope.pSku;
      var brandname        = $scope.pBrandName;
      var brandcode        = $scope.pBrandCode;
      var weight           = $scope.pWeight;
      var length           = $scope.pLength;
      var width            = $scope.pWidth;
      var height           = $scope.pHeight;
      var price            = $scope.pPrice;
      var saleprice        = $scope.pSalePrice;
      var currency         = $scope.pCurrency;
      var saleenddate      = $scope.pSaleEndDate;
      var salestartdate    = $scope.pSaleStartDate;
      var category         = $scope.pLists;
      var attrObject       = JSON.parse($scope.attrselect);
      var attrSelect       = JSON.parse($scope.attrselectvalue);
      var date = new Date();


      var konsolObj = {
        'name': name,
        'group_code' : '',
        'shortdescription': shortdescription,
        'fulldescription': fulldescription,
        'variants': {
          'id': '',
          'quantity': quantity,
          'barcode ': barcode,
          'stockcode': stockcode,
          'sku': sku,
          'attributes': {
              'attribute_id' : attrObject._id,
              'name' : attrObject.name,
              'value' : attrSelect.value,
              'attibute_value_id' : attrSelect.id,
              'is_active' : true,
              'created_date' : date,
              'updated_date' : date
          },
          'price': {
            'id': '',
            'price': price,
            'sale_price': saleprice,
            'currency ': currency,
            'sale_end_date': saleenddate,
            'sale_start_date': salestartdate,
          }
        },
        'is_active': true,
        'brand': {
          'name': brandname,
          'code': brandcode
        },
        'created_date': date,
        'updated_date': date,
        'shipping': {
          'weight': weight,
          'length': length,
          'width': width,
          'height': height,
        },
        'images': {
          'image': {
            'path': '',
            'is_active': true,
            'display_order': 1,
            'created_date': date,
            "type": ''
          }
        },
        'lists': category
      }

      var req = {
        'method': "POST",
        'url': SERVICE_URL + "/catalogue/add",
        'data': konsolObj,
        'headers': {
          'auth': authService.authKey
        }
      }

////////////////////////////// BURADAN DEVAM EDİLECEK


      $http(req)
        .then(function (result) {
          if (result.status = 200) {
            // RESPONSE DONDURULECEK.
            // DONDUR.
            alert('KAYIT BAŞARILI')
          } else {
            alert('Error')
          }
        });

    }
  })


  /*
  *
  * input mask directive
  *
  */

  // .directive('currencyMask', function() {
  //   return {
  //     restrict: 'A',
  //     require: 'ngModel',
  //     link: function(scope, element, attrs, ngModelController) {
        
  //       var formatNumber = function(value) {
       
  //         value = value.toString();
  //         value = value.replace(/[^0-9\.]/g, "");
  //         var parts = value.split('.');
  //         parts[0] = parts[0].replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, "$&,");
  //         if (parts[1] && parts[1].length > 2) {
  //           parts[1] = parts[1].substring(0, 2);
  //         }
         
  //         return parts.join(".");
  //       };
  //       var applyFormatting = function() {
  //         var value = element.val();
  //         var original = value;
  //         if (!value || value.length == 0) {
  //           return
  //         }
  //         value = formatNumber(value);
  //         if (value != original) {
  //           element.val(value);
  //           element.triggerHandler('input')
  //         }
  //       };
  //       element.bind('keyup', function(e) {
  //         var keycode = e.keyCode;
  //         var isTextInputKey =
  //           (keycode > 47 && keycode < 58) || // number keys
  //           keycode == 32 || keycode == 8 || // spacebar or backspace
  //           (keycode > 64 && keycode < 91) || // letter keys
  //           (keycode > 95 && keycode < 112) || // numpad keys
  //           (keycode > 185 && keycode < 193) || // ;=,-./` (in order)
  //           (keycode > 218 && keycode < 223); // [\]' (in order)
  //         if (isTextInputKey) {
  //           applyFormatting();
  //         }
  //       });
  //       element.bind('blur', function(evt) {
  //         if (angular.isDefined(ngModelController.$modelValue)) {
  //           var val = ngModelController.$modelValue.split('.');
  //           if (val && val.length == 1) {
  //             if (val != "") {
  //               ngModelController.$setViewValue(val + '.00');
  //               ngModelController.$render();
  //             }
  //           } else if (val && val.length == 2) {
  //             if (val[1] && val[1].length == 1) {
  //               ngModelController.$setViewValue(val[0] + '.' + val[1] + '0');
  //               ngModelController.$render();
  //             } else if (val[1].length == 0) {
  //               ngModelController.$setViewValue(val[0] + '.00');
  //               ngModelController.$render();
  //             }
  //             applyFormatting();
  //           }
  //         }
  //       })
  //       ngModelController.$parsers.push(function(value) {
  //         if (!value || value.length == 0) {
  //           return value;
  //         }
  //         value = value.toString();
  //         value = value.replace(/[^0-9\.]/g, "");
  //         return value;
  //       });
  //       ngModelController.$formatters.push(function(value) {
  //         if (!value || value.length == 0) {
  //           return value;
  //         }
  //         value = formatNumber(value);
  //         return value;
  //       });
  //     }
  //   };
  // });