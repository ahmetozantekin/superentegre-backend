
// Requires
var moment = require('moment');
var crypto = require('crypto');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var config = require('../config/config');


var MONGO_CONNECTION_STRING = config.dbpath;
var TOKEN_EXPIRE_TIME = config.tokenExpireTime;

module.exports = {

    editUser: function (_token, _username, _password) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("user")
                        .update({ 'token.key': _token }, {
                            $set: {
                                'username': _username,
                                'password': _password
                            }
                        }, { w: 1 }, function (err, results) {
                            if (err) {
                                reject(err)
                            } else {
                                console.log(results.result);
                                resolve(true)
                            }
                        });


                })
            }
        )
    },

    findByToken: function (_token) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("user").find({ "token.key": _token }).toArray(function (err, results) {
                        if (err) {
                            console.log(err);
                            reject(err);
                        } else {
                            resolve(results);
                        }
                    })
                })
            }
        )
    },

    findByName: function (_username) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("user").find({ "username": _username }).toArray(function (err, results) {
                        if (err) {
                            console.log(err);
                            reject(err);
                        } else {
                            resolve(results);
                        }
                    })
                })
            }
        )
    },

    findById: function (_id) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {
                    var mongo_id = new ObjectId(_id);
                    db.collection("user").find({ "_id": mongo_id }).toArray(function (err, results) {
                        if (err) {
                            console.log(err);
                            reject(err);
                        } else {
                            resolve(results);
                        }
                    })
                })
            }
        )
    },

    getAll: function () {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("user").find().toArray(function (err, results) {
                        if (err) {
                            console.log(err);
                            reject(err);
                        } else {
                            resolve(results);
                        }
                    })
                })
            }
        )
    },

    loginUser: function (obj) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("user").find({ 'username': obj.username }).count(function (err, success) {
                        if (err) console.log(err)
                        else {
                            if (success <= 0) {
                                
                                // DB içinde böyle bir kullanici yok.
                                result = {
                                    'status' : false,
                                    'token' : ''
                                }
                                reject(result);

                            } else {

                                // DB icinde username oldugu dogrulamasi yapildi
                                // Usernameye göre sorgu yapıldı
                                db.collection("user").find({ 'username': obj.username })
                                    .toArray(function (err, results) {
                                        if (err) {
                                            result = {
                                                'status' : false,
                                                'token' : ''
                                            }
                                            reject(result);
                                        } else {
                                            
                                            if (results[0].is_active) {
                                                
                                                // Kullanıcı aktif kontrolu yapilir
                                                // Girilen password ile db password kontrol edildi
                                                var dbpass = results[0].password;
                                                if (dbpass == obj.password) {

                                                    
                                                    // Doğru Giriş Yapıldı
                                                    var token     = results[0].token.key;
                                                    var client_id = results[0]._id;

                                                    // dateNow ve expireDate guncellendi
                                                    moment.locale('tr');
                                                    var dateNow = moment().format();
                                                    var dateNowAddHalf = moment().add(30, 'minutes').format();

                                                    db.collection("user")
                                                        .update({ username: obj.username },
                                                         {
                                                            $set: {
                                                                'login': true,
                                                                'token': {
                                                                    'key': token,
                                                                    'created_date': dateNow,
                                                                    'expire_date': dateNowAddHalf
                                                                }
                                                            }
                                                        }, { w: 1 }, function (err, success) {
                                                            if (err) console.log(err)
                                                            else {
                                                                result = {
                                                                    'status' : true,
                                                                    'token' : token,
                                                                    'client_id' : client_id
                                                                }
                                                                resolve(result)
                                                            }
                                                        });

                                                } else {
                                                    // Hatalı Şifre
                                                    
                                                    result = {
                                                        'status' : false,
                                                        'token' : ''
                                                    }
                                                    reject(result);
                                                }
                                            } else {

                                                // Kullanici pasif durumda
                                                // Hesabı silinmiş.

                                                result = {
                                                    'status' : false,
                                                    'token' : ''
                                                }
                                                reject(result)
                                            }
                                        }
                                    })
                            }
                        }
                    })
                })
            }
        )
    },

    createUser: function (obj) {

        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("user").find({ 'username': obj.username }).count(function (err, success) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            if (success > 0) {
                                // DB içinde aynı kullanıcı adında biri daha var.
                                reject('username has already been taken');
                            } else {
                                // Username Boş
                                moment.locale('tr');
                                var dateNow = moment().format();
                                var dateNowAddHalf = moment().add(30, 'minutes').format();
                                var token;

                                // Random token uretiyoruz.
                                require('crypto').randomBytes(16, function (err, buffer) {
                                    token = buffer.toString('hex');

                                    newUser = {
                                        'username': obj.username,
                                        'password': obj.password,
                                        'real_name': obj.real_name,
                                        'membership_start_day' : dateNow,
                                        'email'   : obj.email,
                                        'login': false,
                                        'is_active': true,
                                        'token': {
                                            'key': token,
                                            'created_date': dateNow,
                                            'expire_date': dateNowAddHalf
                                        }
                                    }

                                    // Yeni kullanıcı ekle
                                    db.collection("user").insert(newUser, function (err) {
                                        if (err) {
                                            reject(err);
                                        } else {
                                            resolve(true);
                                        }
                                    });



                                });
                            }
                        }
                    });

                })

            }
        )
    },

    clear: function (_token) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("user").remove({ 'token.key': _token }, function (err) {
                        if (err) reject(err)
                        else {
                            resolve(true)
                        }
                    });

                })
            }
        )
    },

    clearAll: function (_token) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("user").remove({}, function (err) {
                        if (err) reject(err)
                        else {
                            resolve(true)
                        }
                    });

                })
            }
        )
    },

    clearId: function (_id) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {
                    var mongoId = new ObjectId(_id);

                    db.collection("user").update({ '_id': mongoId }, {
                        $set: {
                            "login": false,
                            "is_active": false
                        }
                    }, { w: 1 }, function (err, results) {
                        if (err) {
                            reject(err)
                        } else {
                            resolve(true)
                        }
                    });

                })
            }
        )
    }


}
