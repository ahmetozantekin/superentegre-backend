
// Controller
var moment = require('moment');
var crypto = require('crypto');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var config = require('../config/config');

var MONGO_CONNECTION_STRING = config.dbpath;

module.exports = {

    createCollection: function () {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {
                    db.createCollection("lists");

                })
            }
        )
    },

    getAll: function (token) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("lists").find({client_id : token}).toArray(function (err, results) {
                        if (err) {
                            console.log(err);
                            reject(err);
                        } else {
                            resolve(results);
                        }
                    })
                })
            }
        )
    },

    getAllSubDocument: function (token) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {
                  var k =   db.collection("lists")
                    .aggregate(
                    
                         { $match: { client_id: token } },
                         { $project : { "categories":1 , "_id": 0 } },
                         { $unwind: "$categories" },
                         { $group: { 'categories' : '$categories' } }

                    ).toArray()
                    resolve(k);
                }) 
            }
        )
    },


    findById: function (_cid) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {
                    var mongoId = new ObjectId(_cid);

                    db.collection("lists").find({ '_id': mongoId }).toArray(function (err, results) {
                        if (err) {
                            console.log(err);
                            reject(err);
                        } else {
                            resolve(results);
                        }
                    })
                })
            }
        )
    },

    addListItem: function (obj) {

        moment.locale('tr');
        var dateNow = moment().format();
        var parentlistArray = obj.parent_lists;

        // {
        //     "client_id": "123123",
        //     "categories": [
        //       {
        //         "id": "1",
        //         "category_name": "T-shirt",
        //         "parent_id": ""
        //       },
        //       {
        //         "id": "2",
        //         "category_name": "V-Yaka",
        //         "parent_id": [
        //           "1",
        //           "2"
        //         ]
        //       }
        //     ]
        // }

        var listsObject = {
            'created_date': dateNow,
            'is_active': true,
            'client_id': obj.client_id,
            'categories': obj.categories
            // 'name': obj.name,
        }

        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {
                    db.collection("lists").insert(listsObject, function (err) {
                        if (err) {
                            reject(false);
                        } else {
                            resolve(true);
                        }
                    });

                })
            }
        )
    },

    editListItem: function (_cid, _name) {
        moment.locale('tr');
        var dateNow = moment().format();
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    //Mongo ID compare islemi icin gerekiyor.
                    var mongoId = new ObjectId(_cid);

                    db.collection("lists")
                        .update({ '_id': mongoId }, {
                            $set: {
                                "name": _name,
                                "updated_date": dateNow
                            }
                        }, { w: 1 }, function (err, results) {
                            if (err) {
                                reject(err)
                            } else {
                                resolve(true)
                            }
                        });

                })
            }
        )
    },

    removeListItem: function (_cid) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {
                    var mongoId = new ObjectId(_cid);
                    db.collection("lists").update({ '_id': mongoId }, {
                        $set: {
                            "is_active": false
                        }
                    }, { w: 1 }, function (err, results) {
                        if (err) {
                            reject(err)
                        } else {
                            resolve(true)
                        }
                    });


                })
            }
        )
    },

    clear: function (_token) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("lists").remove({}, function (err) {
                        if (err) reject(err)
                        else {
                            resolve(true)
                        }
                    });

                })
            }
        )
    }

}