angular.module('Giris', [])
    .service('authService', authService);

function authService($http, $cookies) {
    var service = {
        client_id  : client_id,        
        login      : login,
        setLogin   : setLogin,
        logout     : logout, 
        isLoggedIn : isLoggedIn,
        authKey    : authKey,
        signup     : signup
    };
    _loginCheck();
    
    return service;
    ////

    var isLoggedIn = false; 
    var authKey    = ""; 
    var client_id  = "";

    function _loginCheck() { 
        if ($cookies.get("auth") != undefined) {
            service.authKey    = $cookies.get("auth");
            service.client_id  = $cookies.get("client_id")
            service.isLoggedIn = true;
        } else {
            service.authKey    = ""; 
            service.client_id  = "";
            service.isLoggedIn = false;
        }
    }

    function login(username, password) {

        return $http({
            method: 'POST',
            url: "http://localhost:3001/user/signin",
            data: {
                'username' : username,
                'password' : password
            }
        })

    }

    function signup(newUsername, newMail, newPassword, newRealName){
        return $http({
            method : 'POST',
            async: true,
            crossDomain: true,
            url: "http://localhost:3001/user/signup",
            data: {
                'username' : newUsername,
                'email'    : newMail,
                'password' : newPassword,
                'real_name' : newRealName
            }
        
        })
    }

    function setLogin(token , client_id) {
        $cookies.put("auth", token);
        $cookies.put("client_id", client_id)
      
        _loginCheck();
    }


    function logout() {
        $cookies.remove("auth");
        $cookies.remove("client_id")
        _loginCheck();
    }
}