angular.module('Kayit', [])

    .controller('KayitController', function ($rootScope, $scope, $http, $location, authService) {
        $scope.errorbox = false;
        $scope.validerrorbox = false;
        $scope.validerror = '';

        if (authService.isLoggedIn) {
            $location.path('/anasayfa/panel');

        } else {
            $location.path('/user/kayit');
        }
        $scope.closeerror = function () {
            $scope.errorbox = false;
            $scope.validerrorbox = false;
        }
        $scope.signUpUser = function () {
            $scope.validerrorbox = false;
            var newRealName = $scope.n_realname;
            var newUsername = $scope.n_username.toLowerCase().replace(/\s+/g, '');
            var newPassword = $scope.n_password;
            var verifyPass = $scope.n_password2;
            var newMail = $scope.n_mail;

            const MAX_PASSWORD_LENGTH = 16;
            const MIN_PASSWORD_LENGTH = 3;

            // Form Validation Kontrol
            if (!newUsername || !newPassword || !newMail) {
                $scope.validerrorbox = true;
                $scope.validerror = 'Formda eksik bırakmayınız.';

            }
            else {
                if (newPassword.length >= MIN_PASSWORD_LENGTH && newPassword.length <= MAX_PASSWORD_LENGTH) {

                    if (newPassword == verifyPass) {
                        authService.signup(newUsername, newMail, newPassword, newRealName)
                            .then(function (result) {

                                switch (result.data.status_control) {
                                    case '01':
                                        console.log('Kayıt Başarılı Girişe Yönleniyoruz');
                                        $rootScope.correctbox = true;
                                        $location.path('/user/giris');
                                        break;

                                    case '05':
                                        $scope.errorbox = true;
                                        console.log(result.data.message);
                                        break;

                                    case '07':
                                        $scope.errorbox = true;
                                        console.log(result.data.message);
                                        break;

                                    case '03':
                                        $scope.errorbox = true;
                                        console.log(result.data.message);
                                        break;
                                }

                            });
                    } else {
                        // Password 3 karakterden az 16 karakterden fazla olamaz.
                        $scope.validerrorbox = true;
                        $scope.validerror  = 'Şifreler Uyuşmuyor.';
                        $scope.n_password  = '';
                        $scope.n_password2 = '';
                  
                    }

                } else {
                    // Password 3 karakterden az 16 karakterden fazla olamaz.
                    $scope.validerrorbox = true;
                    $scope.validerror = 'Şifre 3 Karakterden Az, 16 Karakterden Fazla Olmamalıdır.';
                    $scope.n_password  = '';
                    $scope.n_password2 = '';
                }

            }

        }

    })
