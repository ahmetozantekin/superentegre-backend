angular.module('Kategoriler', ['multipleSelect'])


    .controller('KategorilerYeniController', function ($scope, $http, $location, authService) {

        const SERVICE_URL = "http://localhost:3001";

        if (authService.isLoggedIn) {
        } else {
          $location.path('/user/giris');
        }

        $scope.ul2 = false;
        $scope.queryResult = [];
        $scope.paramCount = 1;

        $scope.$on('$viewContentLoaded', function () {
        });

      
        $scope.editLists = function (u, p) {
            $scope.load = true;
            var id = u;
            var name = p;

            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/lists/edit?id=" + id + "&name=" + name,
                'headers': {
                    'auth': authService.authKey
                }
            }

            $http(req)
                .then(function (result) {

                    if (result.status = 200) {
                        console.log('Düzenlendi')
                        $scope.getAll();
                    } else {
                        alert('Error')
                    }
                });
        }

        $scope.removeLists = function (u) {
            var id = u;

            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/lists/removebyid?id=" + id,
                'headers': {
                    'auth': authService.authKey
                }
            }

            $http(req)
                .then(function (result) {

                    if (result.status = 200) {
                        console.log('Silindi.')
                        $scope.getAll();
                    } else {
                        alert('Error')
                    }
                });


        }

        $scope.addParam = function () {

            // .directive olarak bi daha yazılır.
            template =
                '<div class="paramelem">' +
                '<div class="form-group">' +
                '<label for="newName' + $scope.paramCount + '"> Kategori Adı: </label>' +
                '<input type="text" class="form-control" id="newName' + $scope.paramCount + '" ng-model="newName+' + $scope.paramCount + '">' +
                '</div>' +
                '<div class="form-group">' +
                '<label for="newParent' + $scope.paramCount + '"> Kategori Parent: </label>' +
                '<input type="text" class="form-control" id="newParent' + $scope.paramCount + '" ng-model="newParent' + $scope.paramCount + '">' +
                '</div>' +
                '</div></hr>';

            tempElement = document.createElement('div');
            tempElement.innerHTML = template;

            document.getElementById('paramlist').appendChild(tempElement.firstChild);
            $scope.paramCount++;
        }

        $scope.newList = function () {
            var listValues = [];
            for (var i = 0; i < $scope.paramCount; i++) {
                listValues.push(
                    {
                        //    kategori-adi-1
                        // önemli-kategori-2
                        //  örnek-kategori-3
                        'id': document.getElementById('newName' + i).value.toLowerCase().trim().split(' ').join('-') + '-' + i,
                        'name': document.getElementById('newName' + i).value,
                        'parent_list': document.getElementById('newParent' + i).value,
                    }
                )
            }
            var listsObject = {
                'client_id': authService.client_id,
                'categories': listValues
            }
            var req = {
                'method': "POST",
                'url': SERVICE_URL + "/lists/add",
                'data': listsObject,
                'headers': {
                    'auth': authService.authKey
                }
            }
            $http(req)
                .then(function (result) {
                    switch (result.data.status_control) {

                        case '01':
                            console.log(result.data.message);
                            $scope.getAll();
                            break;
                        case '03':
                            console.log(result.data.message);
                            break;
                        case '04':
                            console.log(result.data.message);
                            break;
                        case '05':
                            console.log(result.data.message);
                            break;

                    }
                });

        }

    });
