angular.module('Kategoriler', ['multipleSelect'])


    .controller('KategorilerController', function ($scope, $http, $location, authService) {

        const SERVICE_URL = "http://localhost:3001";


        if (authService.isLoggedIn) {
        } else {
            $location.path('/user/giris');
        }

        $scope.ul2 = false;
        $scope.queryResult = [];
        $scope.paramCount = 1;
        $scope.categoryFromClient;

        $scope.$on('$viewContentLoaded', function () {
            $scope.getAll();
        });

        $scope.getAll = function () {
            $scope.load = true;

            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/lists/categories",
                'headers': {
                    'auth': authService.client_id
                }
            }

            $http(req)
                .then(function (result) {

                    switch (result.status) {
                        case 200:
                            $scope.load = false;
                            $scope.ul2 = true;
                            $scope.queryResult = result.data;

                            var jsonCat = [];
                            $scope.queryResult.forEach(function (key) {
                                jsonCat.push(key.categories);
                            })
                            $scope.categoryFromClient = jsonCat;
                            console.log( $scope.categoryFromClient);
                            break;
                        case 404:
                            console.log(result.data.message);
                            break;
                    }

                });
        }

        $scope.editLists = function (u, p) {
            $scope.load = true;
            var id = u;
            var name = p;

            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/lists/edit?id=" + id + "&name=" + name,
                'headers': {
                    'auth': authService.authKey
                }
            }

            $http(req)
                .then(function (result) {

                    if (result.status = 200) {
                        console.log('Düzenlendi')
                        $scope.getAll();
                    } else {
                        alert('Error')
                    }
                });
        }

        $scope.removeLists = function (u) {
            var id = u;
            console.log(id);

            var req = {
                'method': "GET",
                'url': SERVICE_URL + "/lists/removebyid?id=" + id,
                'headers': {
                    'auth': authService.authKey
                }
            }

            $http(req)
                .then(function (result) {

                    if (result.status = 200) {
                        console.log('Silindi.')
                        $scope.getAll();
                    } else {
                        alert('Error')
                    }
                });


        }

        // $scope.addParam = function () {

        //     // .directive olarak bi daha yazılır.
        //     template =
        //         '<div class="paramelem">' +
        //         '<div class="form-group">' +
        //         '<label for="newName' + $scope.paramCount + '"> Kategori Adı: </label>' +
        //         '<input type="text" class="form-control" id="newName' + $scope.paramCount + '" ng-model="newName+' + $scope.paramCount + '">' +
        //         '</div>' +
        //         '<div class="form-group">' +
        //         '<label for="newParent' + $scope.paramCount + '"> Kategori Parent: </label>' +
        //         '<input type="text" class="form-control" id="newParent' + $scope.paramCount + '" ng-model="newParent' + $scope.paramCount + '">' +
        //         '</div>' +
        //         '</div></hr>';

        //     tempElement = document.createElement('div');
        //     tempElement.innerHTML = template;

        //     document.getElementById('paramlist').appendChild(tempElement.firstChild);
        //     $scope.paramCount++;
        // }

        $scope.newList = function () {
            $scope.load = true;
            if ($scope.newParent) {
                var catId = $scope.newParent.split(',')[0];
                var catName = $scope.newParent.split(',')[1];
            } else{
                catId = '';
                catName = '';
            }

            if (!$scope.newName) {
                alert('İsim Alanı Boş Olamaz.');
            } else {
                var listsObject = {
                    'client_id': authService.client_id,
                    'categories': {
                        'is_active': true,
                        'id': authService.client_id + $scope.newName.toLowerCase().trim().split(' ').join('-') + Math.floor(Math.random() * 1000),
                        'name': $scope.newName,
                        'parent_list': catName,
                        'parent_list_id': catId
                    }
                }
                var req = {
                    'method': "POST",
                    'url': SERVICE_URL + "/lists/add",
                    'data': listsObject,
                    'headers': {
                        'auth': authService.authKey
                    }
                }
                $http(req)
                    .then(function (result) {
                        switch (result.data.status_control) {

                            case '01':
                                console.log(result.data.message);
                                $scope.load = false;
                                $location.path('/lists/tumunugor');
                                break;
                            case '03':
                                console.log(result.data.message);
                                break;
                            case '04':
                                console.log(result.data.message);
                                break;
                            case '05':
                                console.log(result.data.message);
                                break;

                        }
                    });
            }

        }

    });
