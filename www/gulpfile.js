var gulp = require('gulp');
var notify = require("gulp-notify");
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var browserSync = require('browser-sync').create();
var watch = require('gulp-watch');
 

gulp.task('concatModules', function () {
    gulp.src('app/modules/**/*.js')
        .pipe(concat('modules.js'))
        .pipe(gulp.dest('app/core/'))
        .pipe(notify({ message: 'concat modules' }));
});

gulp.task('concatServices', function () {
    gulp.src('app/services/*.js')
        .pipe(concat('services.js'))
        .pipe(gulp.dest('app/core/'))
        .pipe(notify({ message: 'concat service' }));
});

gulp.task('sass', function () {
    return gulp.src('style/style.scss')
        .pipe(sass().on('error', function (err) {
            console.log(err);
        }))
        .pipe(gulp.dest('dist/css'))
});

gulp.task('serve', function () {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});

gulp.task('watch', function () {
    gulp.watch('app/modules/**/*.js', ['concatModules']);
    gulp.watch('app/services/*.js', ['concatServices']);
    gulp.watch('style/*.scss', ['sass']);
});


gulp.task('default', ['serve', 'concatModules', 'concatServices', 'sass', 'watch']);
