
// Token Control
var moment = require('moment');
var crypto = require('crypto');
var MongoClient = require('mongodb').MongoClient;
var config = require('../config/config');

var MONGO_CONNECTION_STRING = config.dbpath;
var TOKEN_EXPIRE_TIME = config.tokenExpireTime;
var JOKER_TOKEN = config.jokerToken;

module.exports = {

    control: function (getToken) {

        var _token = getToken;

        // Header içinde token atan user için token kontrol ediliyor
        // DB içinde bu token varsa metotlara erişim izni oluyor
        // Bu token sahibi user için Date ve Expire_Date de değişiyor.
        return new Promise(
            function (resolve, reject) {

                // !!!! Burası Silinecek
                if (_token == JOKER_TOKEN) {
                    resolve(true);
                }
                 // !!!! Burası Silinecek
                 
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("user").find({ 'token.key': _token })
                        .toArray(function (err, results) {
                            if (err) {
                                reject(err);
                            } else {
                                // Token Doğru
                                // Expire Date zamani kontrol ediliyor

                                if (results.length <= 0) {

                                    reject('Auth Hatası')

                                } else {

                                    moment.locale('tr');

                                    var dateNow = moment(moment().format());
                                    var expired = moment(results[0].token.expire_date);
                                    var diffM = dateNow.diff(expired, 'minutes');
                                    var expireDiffrence = ((-1) * parseInt(diffM));


                                    // Expire Date ve Date Now Arasındaki dakika farkı
                                    // 30'dan büyükse TOKEN_KEY ve login false olur.
                                    // Ama 30'dan küçükse aktif kullanıcıdır ve TOKEN devam eder.
                                    if (expireDiffrence < TOKEN_EXPIRE_TIME && expireDiffrence >= 0) {
                                        resolve(true);

                                        // Date ve Expire_Date Değişiyor
                                        db.collection("user")
                                            .update({ 'token.key': _token },
                                            {
                                                $set: {
                                                    'login': true,
                                                    'token': {
                                                        'key': _token,
                                                        'created_date': dateNow.format(),
                                                        'expire_date': dateNow.add(30, 'minutes').format()
                                                    }
                                                }
                                            }, { w: 1 }, function (err, success) {
                                                if (err) console.log(err)
                                                else {
                                                    resolve(true)
                                                }
                                            });

                                    } else {

                                        reject('Token Expire Timeout.');

                                        // Logout olur.
                                        db.collection("user")
                                            .update({ 'token.key': _token },
                                            {
                                                $set: {
                                                    'login': false,
                                                    'token': {
                                                        'key': _token,
                                                        'created_date': dateNow.format(),
                                                        'expire_date': dateNow.format(),
                                                    }
                                                }
                                            }, { w: 1 }, function (err, success) {
                                                if (err) console.log(err)
                                                else {
                                                    resolve(true)
                                                }
                                            });
                                    }

                                }


                            }
                        })
                })
            }
        )
    }

}