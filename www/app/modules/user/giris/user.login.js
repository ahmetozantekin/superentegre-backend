angular.module('Giris')

    .controller('GirisController', function ($rootScope, $scope, $http, $location, authService) {
        $scope.n_username = '';
        $scope.n_password = '';
        $scope.errorbox = false;

        if( authService.isLoggedIn ){
            $location.path('/anasayfa/panel');
            
        } else {
            $location.path('/user/giris'); 
        }

        $scope.closeerror = function(){
            $scope.errorbox = false;
            $rootScope.correctbox = false;
        }
        $scope.loginUser = function () {
            var newUsername = $scope.n_username;
            var newPassword = $scope.n_password;

            authService.login(newUsername, newPassword)
                .then(function (result) { 

                    switch(result.data.status_control){

                        case '01' : 
                            // console.log(result.data.message);
                            authService.setLogin( result.data.token , result.data.client_id);
                            $location.path('/user/profil');
                            break;
                        case '03' : 
                            // console.log(result.data.message);
                            $scope.errorbox = true;
                            $rootScope.correctbox = false;
                            break;

                    }

                });
        }

        $scope.logoutUser = function(){
            authService.logout();
        }
    });