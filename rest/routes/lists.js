// Router
var express = require('express');
var router = express.Router();

var token = require('../controllers/token');
var lists = require('../controllers/lists');

/*
    01: invalid data
    02: username/password wrong
    03: user doesnt exists
    04: invalid token
*/
router.get('/', function (req, res) {
    // lists.createCollection();
    res.status(200).json({
        'status': res.statusCode,
        'status_control': '01',
        'success': true,
        'message': 'Successful'
    });
})


/**
 * 
 * @api {Get} /lists/all Get All List
 * @apiName Get All List
 * @apiGroup Lists
 * @apiHeader {String} auth Users client id
 * @apiVersion 0.0.1
 * 
 */
router.get('/all', function (req, res) {
    var client = JSON.parse(JSON.stringify(req.headers)).auth;
    
    lists.getAll(client)
        .then(function (result) {
            res.json(result);
        }) 
        .catch(function (err) {
            res.status(404).json({
                'status': res.statusCode,
                'status_control': '05',
                'success': false,
                'message': 'Error'
            });
        })

})

/**
 * 
 * @api {Get} /lists/categories Get All Categories
 * @apiName Get All Categories for User
 * @apiGroup Lists
 * @apiHeader {String} auth Users client id
 * @apiVersion 0.0.1
 * 
 */
router.get('/categories', function (req, res) {
    var client = JSON.parse(JSON.stringify(req.headers)).auth;
    
    lists.getAllSubDocument(client)
        .then(function (result) {
            res.json(result);
        }) 
        .catch(function (err) {
            res.status(404).json({
                'status': res.statusCode,
                'status_control': '05',
                'success': false,
                'message': 'Error'
            });
        })

})


/**
 * 
 * @api {Post} /lists/add Add List
 * @apiName Add List
 * @apiGroup Lists
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 * 
 * @apiParam {Array} data, parent list array
 */
router.post('/add', function (req, res) {

    var addObj = req.body;
    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth

    token.control(auth_token)
        .then(function (result) {
            if (result) {
                lists.addListItem(addObj)
                    .then(function (result) {
                        if (result) {
                            res.status(200).json({
                                'status': res.statusCode,
                                'status_control': '01',
                                'success': true,
                                'message': 'List save successful'
                            });
                        }
                    })
                    .catch(function (err) {
                        res.status(404).json({
                            'status': res.statusCode,
                            'status_control': '05',
                            'success': false,
                            'message': 'Error'
                        });
                    })
            } else {
                res.status(401).json({
                    'status': res.statusCode,
                    'status_control': '04',
                    'success': false,
                    'message': 'Authorization Error'
                });
            }
        })
        .catch(function (err) {
            res.status(401).json({
                'status': res.statusCode,
                'status_control': '04',
                'success': false,
                'message': 'Authorization Error'
            });
        })

})


/**
 * 
 * @api {Get} /lists/edit Edit List
 * @apiName Edit List
 * @apiGroup Lists
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 * 
 * @apiParam {String} name unique list name
 * @apiParam {String} id list ID
 */
router.get('/edit', function (req, res) {

    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth
    var _name = req.query.name;
    var _id = req.query.id;

    token.control(auth_token)
        .then(function (result) {
            if (result) {
                lists.editListItem(_id, _name)
                    .then(function (result) {
                        if (result) {
                            res.status(200).json({
                                'status': res.statusCode,
                                'status_control': '01',
                                'success': true,
                                'message': 'List edit successful'
                            });
                        }
                    })
                    .catch(function (err) {
                        res.status(404).json({
                            'status': res.statusCode,
                            'status_control': '03',
                            'success': false,
                            'message': 'Error'
                        });
                    })
            } else {
                res.status(401).json({
                    'status': res.statusCode,
                    'status_control': '04',
                    'success': false,
                    'message': 'Authorization Error'
                });
            }
        })
        .catch(function (err) {
            res.status(401).json({
                'status': res.statusCode,
                'status_control': '04',
                'success': false,
                'message': 'Authorization Error'
            });
        })

})


/**
 * 
 * @api {Get} /lists/removebyid Remove List By Id
 * @apiName Remove List By Id
 * @apiGroup Lists
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 * 
 * @apiParam {String} id list ID
 */
router.get('/removebyid', function (req, res) {
    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth;
    var _id = req.query.id;

    token.control(auth_token)
        .then(function (result) {
            if (result) {
                lists.removeListItem(_id)
                    .then(function (result) {
                        if (result) {
                            res.status(200).json({
                                'status': res.statusCode,
                                'success': true,
                                'message': 'List remove successful'
                            });
                        }
                    })
                    .catch(function (err) {
                        res.status(404).json({
                            'status': res.statusCode,
                            'success': false,
                            'message': 'Error'
                        });
                    })
            } else {
                res.status(401).json({
                    'status': res.statusCode,
                    'success': false,
                    'message': 'Authorization Error'
                });
            }
        })
        .catch(function (err) {
            res.status(401).json({
                'status': res.statusCode,
                'success': false,
                'message': 'Authorization Error'
            });
        })

})


/**
 * 
 * @api {Delete} /lists/removeall Remove All List
 * @apiName Remove All List
 * @apiGroup Lists
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 * 
 */
router.get('/removeall', function (req, res) {
    lists.clear()
        .then(function (result) {
            res.status(200).json({
                'status': res.statusCode,
                'status_control': '01',
                'success': true,
                'message': 'Removed All Lists'
            });
        })
        .catch(function (err) {
            res.status(404).json({
                'status': res.statusCode,
                'success': false,
                'message': 'Error'
            });
        })
})


/**
 * 
 * @api {Get} /lists/findbyid Find List By Id
 * @apiName Find List By Id
 * @apiGroup Lists
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 * 
 * @apiParam {String} id list ID
 */
router.get('/findbyid', function (req, res) {

    var id = req.query.id;
    lists.findById(id)
        .then(function (result) {
            res.json(result);
        })
        .catch(function (err) {
            res.status(404).json({
                'status': res.statusCode,
                'success': false,
                'message': 'Error'
            });
        })

})


module.exports = router;