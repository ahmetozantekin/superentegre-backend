angular.module('Router', ['ui.router'])


    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider
            .otherwise('/user/giris');


        $stateProvider

            // Parents
            .state('anasayfa', {
                abstract: true,
                url: '/anasayfa'
            })

            .state('user', {
                abstract: true,
                url: '/user'
            })

            .state('lists', {
                abstract: true,
                url: '/lists'
            })

            .state('product', {
                abstract: true,
                url: '/product'
            })

            // Childs
            .state('panel', {
                url: '/panel',
                parent: 'anasayfa',
                templateUrl: 'app/modules/anasayfa/views/panel.html'
            })

            .state('home', {
                url: '/',
                parent: 'user',
                controller: 'GirisController',
                templateUrl: 'app/modules/user/giris/views/giris.html'
            })

            .state('kayit', {
                url: '/kayit',
                parent: 'user',
                controller: 'KayitController',
                templateUrl: 'app/modules/user/kayit/views/kayit.html'
            })

            .state('giris', {
                url: '/giris',
                parent: 'user',
                controller: 'GirisController',
                templateUrl: 'app/modules/user/giris/views/giris.html'
            })

            .state('profil', {
                url: '/profil',
                parent: 'user',
                controller: 'TumunuGorController',
                templateUrl: 'app/modules/user/tumunugor/views/tumunugor.html'
            })


            .state('kategoriekle', {
                url: '/yeni',
                parent: 'lists',
                controller: 'KategorilerController',
                templateUrl: 'app/modules/lists/kategoriler/views/kategoriekle.html'
            })

            .state('kategoriler', {
                url: '/tumunugor', 
                parent: 'lists',
                controller: 'KategorilerController',
                templateUrl: 'app/modules/lists/kategoriler/views/kategoriler.html'
            })


            .state('urunler', {
                url: '/urunler',
                parent: 'product',
                controller: 'UrunlerController',
                templateUrl: 'app/modules/product/urunler/views/urunler.html'
            })

            .state('ozellikler', {
                url: '/ozellikler',
                parent: 'product',
                controller: 'OzellikController',
                templateUrl: 'app/modules/product/ozellik/views/ozellik.html'
            })

            .state('hata', {
                url: '/hata',
                controller: 'GirisController',
                templateUrl: 'app/modules/hata/views/hata.html'
            });


    });
