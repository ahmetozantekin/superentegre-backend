var express = require('express');
var router = express.Router();

var token = require('../controllers/token');
var user = require('../controllers/user');
var validator = require('validator')


router.get('/', function (req, res) {
    //user.createCollection();
    res.json({
        'status_code': res.statusCode,
        'status_control' : '01',        
        'success': true,
        'message': 'Success'
    });
})

/**
 * 
 * @api {Post} /user/signup  User Signup
 * @apiName User Signup
 * @apiGroup User
 * @apiVersion 0.0.1
 *
 * @apiParam {String} username a unique username
 * @apiParam {String} password users password
 */
router.post('/signup', function (req, res) {
   
    var addObj = req.body;

    if(validator.isEmail(addObj.email)){
        // Validation - Email
        // x@y.z
        
        user.createUser(addObj)
        .then(function (result) {
            if (result) {
                res.status(200).json({
                    'status_code': res.statusCode,
                    'status_control' : '01',
                    'success': true,
                    'message': 'User saved'
                });
            }
        })
        .catch(function (err) {
            res.status(200).json({
                'status_code': res.statusCode,
                'status_control' : '05',
                'success': false,
                'message': "Signup error"
            })
        })
    }  else{
        res.status(200).json({
            'status_code': res.statusCode,
            'status_control' : '07',
            'success': false,
            'message': "Validation error"
        })
    }
 

})



/**
 * 
 * @api {Post} /user/signin  User Signin
 * @apiName User Signin
 * @apiGroup User
 * @apiVersion 0.0.1
 *
 * @apiParam {String} username a unique username
 * @apiParam {String} password users password
 */
router.post('/signin', function (req, res) {
    // var _username = req.query.username;
    // var _password = req.query.password;
    var addObj = req.body;

    user.loginUser(addObj)
        .then(function (result) {
            if (result.status ) {
                res.status(200).json({ 
                    'status_code': res.statusCode,
                    'status_control' : '01',
                    'token' : result.token, 
                    'client_id': result.client_id,            
                    'success': true,
                    'message': 'Login successful'
                });
            } else {
                res.status(200).json({
                    'status_code': res.statusCode,
                    'status_control' : '03',           
                    'success': false,
                    'message': "User Not Found"
                });
            }
        })
        .catch(function (err) {
            console.log(err);
            res.status(200).json({
                'status_code': res.statusCode,
                'status_control' : '03',           
                'success': false,
                'message': "User Not Found"
            });
        })

})



/**
 * 
 * @api {Get} /user/all  Get All User
 * @apiName Get All User
 * @apiGroup User
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 */
router.get('/all', function (req, res) {
    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth

    token.control(auth_token)
        .then(function (result) {
            if (result) {
                user.getAll()
                    .then(function (result) {
                        res.json(result);
                    })
                    .catch(function (err) {
                        res.status(404).json({
                            'status_code': res.statusCode,
                            'status_control' : '05',                            
                            'success': false,
                            'message': "Error"
                        });
                    })
            } else {
                res.status(401).json({
                    'status_code': res.statusCode,
                    'status_control' : '04',                
                    'success': false,
                    'message': 'Authorization Error'
                });
            }
        })
        .catch(function (err) {
            res.status(401).json({
                'status_code': res.statusCode,
                'status_control' : '04',                
                'success': false,
                'message': 'Authorization Error'
            });
        })
})



/**
 * 
 * @api {Get} /user/findbytoken  Get User From Token
 * @apiName Get User From Token
 * @apiGroup User
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 *
 */
router.get('/findbytoken', function (req, res) {
    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth

    token.control(auth_token)
        .then(function (result) {
            if (result) {
                user.findByToken(auth_token)
                    .then(function (result) {
                        res.json(result);
                    })
                    .catch(function (err) {
                        res.status(404).json({
                            'status_code': res.statusCode,
                            'success': false,
                            'status_control' : '05',                
                            'message': "User Not Found"
                        });
                    })
            } else {
                res.status(401).json({
                    'status_code': res.statusCode,
                    'status_control' : '04',                
                    'success': false,
                    'message': 'Authorization Error'
                });
            }
        })
        .catch(function (err) {
            res.status(401).json({
                'status_code': res.statusCode,
                'status_control' : '04',
                'success': false,
                'message': 'Authorization Error'
            });
        })

})



/**
 * 
 * @api {Get} /user/findbyname  Get User From Username
 * @apiName Get User From Username
 * @apiGroup User
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 *
 * @apiParam {String} username unique username
 */
router.get('/findbyname', function (req, res) {
    var usrname = req.query.username

    user.findByName(usrname)
        .then(function (result) {
            if (result.length > 0) {
                res.json(result);
            } else {
                res.status(404).json({
                    'status_code': res.statusCode,
                    'status_control' : '03',                
                    'success': false,
                    'message': "User Not Found"
                });
            }
        })
        .catch(function (err) {
            res.status(404).json({
                'status_code': res.statusCode,
                'success': false,
                'status_control' : '03',                
                'message': 'User Not Found'
            });
        })

})


/**
 * 
 * @api {Get} /user/findbyid  Get User From User Id
 * @apiName Get User From User Id
 * @apiGroup User
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 *
 * @apiParam {String} id unique user id
 */
router.get('/findbyid', function (req, res) {
    var client_id = req.query.id

    user.findById(client_id)
        .then(function (result) {
            if (result.length > 0) {
                res.json(result);
            } else {
                res.status(404).json({
                    'status_code': res.statusCode,
                    'status_control' : '03',                
                    'success': false,
                    'message': "User Not Found"
                });
            }
        })
        .catch(function (err) {
            res.status(404).json({
                'status_code': res.statusCode,
                'success': false,
                'status_control' : '03',                
                'message': 'User Not Found'
            });
        })

})

/**
 * 
 * @api {Get} /user/edit  Edit User Login Info
 * @apiName Edit User Login Info
 * @apiGroup User
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 *
 * @apiParam {String} username unique username
 * @apiParam {String} password users password
 */
router.get('/edit', function (req, res) {
    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth
    var _username = req.query.username;
    var _password = req.query.password;

    token.control(auth_token)
        .then(function (result) {
            if (result) {
                user.editUser(auth_token, _username, _password)
                    .then(function (result) {
                        if (result) {
                            console.log('edit: true')
                            res.status(200).json({
                                'status_code': res.statusCode,
                                'status_control' : '01',
                                'success': false,
                                'message': 'User edit successful'
                            });
                        }
                    })
                    .catch(function (err) {
                        res.status(404).json({
                            'status_code': res.statusCode,
                            'status_control' : '05',
                            'success': false,
                            'message': "Edit error"
                        });
                    })
            } else {
                res.status(401).json({
                    'status_code': res.statusCode,
                    'status_control' : '04',                
                    'success': false,
                    'message': 'Authorization Error'
                });
            }
        })
        .catch(function (err) {
            res.status(401).json({
                'status_code': res.statusCode,
                'status_control' : '04',                
                'success': false,
                'message': 'Authorization Error'
            });
        })

})



/**
 * 
 * @api {Get} /user/removebytoken  Remove User From Token
 * @apiName Remove User From Token
 * @apiGroup User
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 */
router.get('/removebytoken', function (req, res) {
    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth


    user.clear(auth_token)
        .then(function (result) {
            if (result) {
                console.log('remove: true')
                res.status(200).json({
                    'status_code': res.statusCode,
                    'status_control' : '01',                
                    'success': false,
                    'message': 'Remove user successful'
                });
            }
        })
        .catch(function (err) {
            res.status(404).json({
                'status_code': res.statusCode,
                'status_control' : '03',                
                'success': false,
                'message': "User Not Found"
            });
        })

})



/**
 * 
 * @api {Get} /user/removebyid Remove User From Id
 * @apiName Remove User From Id
 * @apiGroup User
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 * 
 * @apiParam {String} id unique user id
 */
router.get('/removebyid', function (req, res) {
    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth
    var id = req.query.id

    token.control(auth_token)
        .then(function (result) {
            if (result) {
                user.clearId(id)
                    .then(function (result) {
                        if (result) { 
                            res.status(200).json({
                                'status_code': res.statusCode,
                                'status_control' : '01',
                                'success': false,
                                'message': 'Remove user successful'
                            });
                        } 
                    }) 
                    .catch(function (err) {
                        res.status(404).json({
                            'status_code': res.statusCode,
                            'status_control' : '03',                
                            'success': false,
                            'message': "User Not Found"
                        });
                    })
            } else {
                res.status(401).json({
                    'status_code': res.statusCode,
                    'status_control' : '04',
                    'success': false,
                    'message': 'Authorization Error'
                });
            }

        })
        .catch(function (err) {
            res.status(404).json({
                'status_code': res.statusCode,
                'status_control' : '04',                
                'success': false,
                'message': "User Not Found"
            });
        })
})



/**
 *  @Private Route 
 */
router.get('/clearall', function (req, res) {
    user.clearAll()
        .then(function (result) {
            if (result) {
                res.status(200).json({
                    'status_code': res.statusCode,
                    'status_control' : '01',
                    'success': false,
                    'message': 'Remove user successful'
                });
            }
        })
        .catch(function (err) {
            res.status(404).json({
                'status_code': res.statusCode,
                'status_control' : '03',                
                'success': false,
                'message': "User Not Found"
            });
        })

})


module.exports = router;