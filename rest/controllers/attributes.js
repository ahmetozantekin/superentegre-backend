
// Controller
var moment = require('moment');
var crypto = require('crypto');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var config = require('../config/config');

var MONGO_CONNECTION_STRING = config.dbpath;

module.exports = {

    createCollection: function () {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {
                    db.createCollection("attributes");
                })
            }
        )
    },

    getAll: function () {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("attributes").find().toArray(function (err, results) {
                        if (err) {
                            console.log(err);
                            reject(err);
                        } else {
                            resolve(results);
                        }
                    })
                })
            }
        )
    },

    findById: function (_cid) {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {
                    var mongoId = new ObjectId(_cid);

                    db.collection("attributes").find({ '_id': mongoId }).toArray(function (err, results) {
                        if (err) {
                            console.log(err);
                            reject(err);
                        } else {
                            resolve(results);
                        }
                    })
                })
            }
        )
    },
    
    clearAll: function(){
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("attributes").remove({}, function (err) {
                        if (err) reject(err)
                        else {
                            resolve(true)
                        }
                    });

                })
            }
        )
    },

    addListItem: function (obj) {

        moment.locale('tr');
        var dateNow = moment().format(); 
        

        // {
        //     "id": "",
        //     "name": "BEDEN",
        //     "description": "BEDEN DEGERLERI",
        //     "display_order": 1,
        //     "is_active": true,
        //     "created_date": "",
        //     "values": [
        //       {
        //         "id": "",
        //         "name": "XS",
        //         "value": "X Small",
        //         "description": "",
        //         "is_active": true
        //       },
        //       {
        //         "id": "",
        //         "name": "M",
        //         "value": "Medium",
        //         "description": "",
        //         "is_active": true
        //       }
        //     ]
        //   }
        

        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("attributes").find({ 'name': obj.name }).count(function (err, success) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            if (success > 0) {
                                // DB içinde aynı attr adında biri daha var.
                                reject('attr name has already been taken');
                            } else {

                                // Yeni list ekle
                                db.collection("attributes").insert(obj, function (err) {
                                    if (err) {
                                        reject(err);
                                    } else {
                                        resolve(true);
                                    }
                                });
                            }
                        }
                    });

                })
            }
        )
    }


}