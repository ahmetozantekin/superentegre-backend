// Requires
var express = require('express');
var morgan = require('morgan')
var bodyParser = require('body-parser');
var MongoClient = require('mongodb').MongoClient;
var config = require('./config/config');


// Routes
var user_router = require('./routes/user');
var lists_router = require('./routes/lists');
var attributes_router = require('./routes/attributes');
var catalogue_router = require('./routes/catalogue');


// Instance from ExpressJS
var app = express();
app
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({ "extended": false }));


// Logging
app
    .use(morgan('tiny'))


// Cors
app
    .use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
        res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin, Accept, auth, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
        res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate');
        res.setHeader('Pragma', 'no-cache');
        next();
    });


// Routing
app
    .use('/apidoc', express.static('doc'))
    .use('/user', user_router)
    .use('/lists', lists_router)
    .use('/attributes', attributes_router)
    .use('/catalogue', catalogue_router)
    .use(function (req, res, next) {
        res.json({ 'server_status': res.statusCode, 'success': false, 'message': 'invalid route, read apidocs in /apidoc path' });
        next();
    });


// Listen
app
    .listen(config.port, function () { console.log(config.port + ' Dinleniyor') })
