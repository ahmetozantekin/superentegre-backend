'use strict';

angular.module('SuperEntegreWebApp', [
    'angular-md5', 
    'ngCookies',
    'Router', 
    'Kayit',
    'Giris',
    'TumunuGor',
    'Kategoriler',
    'Urunler',
    'Ozellik'
])
