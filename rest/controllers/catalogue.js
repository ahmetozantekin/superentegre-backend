
// Controller
var moment = require('moment');
var crypto = require('crypto');
var MongoClient = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var config = require('../config/config');

var MONGO_CONNECTION_STRING = config.dbpath;

module.exports = {

    createCollection: function () {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {
                    db.createCollection("catalogue");
                })
            }
        )
    },

    getAll: function () {
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("catalogue").find().toArray(function (err, results) {
                        if (err) {
                            console.log(err);
                            reject(err);
                        } else {
                            resolve(results);
                        }
                    })
                })
            }
        )
    },

    addCatalogueItem: function (obj) {

        moment.locale('tr');
        var dateNow = moment().format();
        console.log(obj);
        // {
        //     "id": "",
        //     "group_code": "MODEL KODU",
        //     "name": "",
        //     "short_description": "",
        //     "full_description": "",
        //     "variants":
        //       {
        //         "id": "",
        //         "quantity": "5",
        //         "barcode": "sdfsfsdfsf",
        //         "stock_code": "dfsdfsdf",
        //         "sku": "sdfsdfdsfdsf",
        //         "attributes": {
        //           "attribute_id": "attributes.id",
        //           "name": "RENK",
        //           "value": "SARI",
        //           "attibute_value_id": "attributes.values.id",
        //           "is_active": true,
        //           "created_date": "",
        //           "updated_date": ""
        //         },
        //         "price": {
        //           "price": 85,
        //           "sale_price": 62,
        //           "currency": "",
        //           "id": "", 
        //           "sale_end_date": "",
        //           "sale_start_date": ""
        //         }
        //       },
        //     "is_active": true,
        //     "brand": {
        //       "name": "",
        //       "code": ""
        //     },
        //     "created_date": "",
        //     "updated_date": "",
        //     "shipping": {
        //       "Weight": "",
        //       "Length": "",
        //       "Width": "",
        //       "Height": ""
        //     },
        //     "images": {
        //       "path": "",
        //       "is_actice": true,
        //       "display_order": 1,
        //       "created_date": "",
        //       "updated_date": "",
        //       "type": ""
        //     },
        //     "lists": [
        //       {}
        //     ]
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    // Yeni urun ekle
                    db.collection("catalogue").insert(obj, function (err) {
                        if (err) {
                            reject(err);
                        } else {
                            
                            resolve(true);
                        }
                    });

                })
            }
        )
    },

    clearAll: function(){
        return new Promise(
            function (resolve, reject) {
                MongoClient.connect(MONGO_CONNECTION_STRING, function (err, db) {

                    db.collection("catalogue").remove({}, function (err) {
                        if (err) reject(err)
                        else {  
                            resolve(true)
                        }
                    });

                })
            }
        )
    },

} 