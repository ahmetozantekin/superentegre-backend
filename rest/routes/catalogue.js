// Router
var express = require('express');
var router = express.Router();

var token = require('../controllers/token');
var catalogue = require('../controllers/catalogue');

router.get('/createCollection', function (req, res) {
    // catalogue.createCollection();
    res.send('Server status: ' + res.statusCode)
})

/**
 * 
 * @api {Get} /catalogue/all Get All Catalogue
 * @apiName Get All Catalogue
 * @apiGroup Catalogues
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 * 
 */
router.get('/all', function (req, res) {
    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth

    token.control(auth_token)
        .then(function (result) {
            if (result) {

                catalogue.getAll()
                    .then(function (result) {
                        res.json(result);
                    })
                    .catch(function (err) {
                        console.log(err)
                    })
            }
        })
        .catch(function (err) {
            console.log(err)
        })


})


/** 
 * 
 * @api {Post} /catalogue/add Add Catalogue
 * @apiName Add Catalogue
 * @apiGroup Catalogues
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 * 
 * @apiParam {Object} data Catalogue Data Object
 */
router.post('/add', function (req, res) {
    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth

    token.control(auth_token)
        .then(function (result) {
            if (result) {
                catalogue.addCatalogueItem(req.body)
                    .then(function (result) {
                        if (result) {
                            res.status(200).json({
                                'status': res.statusCode,
                                'status_control': '01',
                                'success': true,
                                'message': 'Successful'
                            });
                        }
                    })
                    .catch(function (err) {
                        res.status(404).json({
                            'status': res.statusCode,
                            'status_control': '05',
                            'success': false,
                            'message': 'Error'
                        });
                    })

            }
        })
        .catch(function (err) {
            res.status(401).json({
                'status': res.statusCode,
                'status_control': '04',
                'success': false,
                'message': 'Authorization Error'
            });
        })

})


/** 
 * @private
 * @api {Delete} /catalogue/clearall Clear All Catalogue
 * @apiName Clear All Catalogue
 * @apiGroup Catalogues
 * @apiHeader {String} auth Users unique token
 * @apiVersion 0.0.1
 * @apiParam {Object} data Catalogue Data Object
 */
router.post('/clearall', function (req, res) {
    var auth_token = JSON.parse(JSON.stringify(req.headers)).auth

    token.control(auth_token)
        .then(function (result) { 
            if (result) {
                catalogue.clearAll()
                    .then(function (result) {
                        if (result) {
                            res.status(200).json({
                                'status': res.statusCode,
                                'status_control': '01',
                                'success': true,
                                'message': 'Successful'
                            });
                        }
                    })
                    .catch(function (err) {
                        res.status(404).json({
                            'status': res.statusCode,
                            'status_control': '05',
                            'success': false,
                            'message': 'Error'
                        });
                    })

            }
        })
        .catch(function (err) {
            res.status(401).json({
                'status': res.statusCode,
                'status_control': '04',
                'success': false,
                'message': 'Authorization Error'
            });
        })

})
module.exports = router;